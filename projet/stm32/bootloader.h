/**
  ******************************************************************************
  * @file    bootloader.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   This file contains all the functions prototypes for the bootloader
  *          and USART library.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _BOOTLOADER_H_
#define _BOOTLOADER_H_

/* Includes ------------------------------------------------------------------*/
#include "oslmic.h"
#include "stm32l1xx_conf.h"

/* Exported constants --------------------------------------------------------*/

/** @addtogroup STM32L1xx_StdPeriph_Driver
  * @{
  */

/** @addtogroup BOOTLOADER
  * @{
  */

/** @defgroup Device_ID_Constants
* @{
*/

//Application Note AN2606, Table 68, page 139
#define PID 0x416
#define BL_ID 0x20

/**
  * @}
  */ 

/** @defgroup Device_Address_Constants
* @{
*/
#if PID == 0x440 || PID == 0x444
  #define SYS_MEM_ADDRESS 0x1FFFEC00 
  #define RAM_MEM_ADDRESS 0x20000800
#elif PID == 0x445
  #define SYS_MEM_ADDRESS 0x1FFFC400 
  #define RAM_MEM_ADDRESS 0x00000000 //N.A.
#elif PID == 0x448
  #define SYS_MEM_ADDRESS 0x1FFFC800 
  #define RAM_MEM_ADDRESS 0x20001800
#elif PID == 0x412 || PID == 0x410 || PID == 0x414 || PID == 0x420 || PID == 0x428
  #define SYS_MEM_ADDRESS 0x1FFFF000 
  #define RAM_MEM_ADDRESS 0x20000200
#elif PID == 0x418
  #define SYS_MEM_ADDRESS 0x1FFFB000 
  #define RAM_MEM_ADDRESS 0x20001000
#elif PID == 0x430
  #define SYS_MEM_ADDRESS 0x1FFFE000 
  #define RAM_MEM_ADDRESS 0x20000800
#elif PID == 0x411
  #define SYS_MEM_ADDRESS 0x1FFF0000 
  #define RAM_MEM_ADDRESS 0x20002000
#elif PID == 0x432 && BL_ID == 0x41
  #define SYS_MEM_ADDRESS 0x1FFFD800 
  #define RAM_MEM_ADDRESS 0x20001400
#elif PID == 0x432 && BL_ID == 0x50
  #define SYS_MEM_ADDRESS 0x1FFFD800 
  #define RAM_MEM_ADDRESS 0x20001400
#elif PID == 0x422
  #define SYS_MEM_ADDRESS 0x1FFFD800 
  #define RAM_MEM_ADDRESS 0x20001400
#elif PID == 0x439 || PID == 0x438
  #define SYS_MEM_ADDRESS 0x1FFFD800 
  #define RAM_MEM_ADDRESS 0x20001000
#elif PID == 0x413 && BL_ID == 0x31
  #define SYS_MEM_ADDRESS 0x1FFF0000 
  #define RAM_MEM_ADDRESS 0x20002000
#elif PID == 0x413 && BL_ID == 0x90
  #define SYS_MEM_ADDRESS 0x1FFF0000 
  #define RAM_MEM_ADDRESS 0x20003000
#elif PID == 0x419 || PID == 0x423 || PID == 0x433 || PID == 0x431 || PID == 0x421
  #define SYS_MEM_ADDRESS 0x1FFF0000 
  #define RAM_MEM_ADDRESS 0x20003000
#elif PID == 0x417 || PID == 0x429 || PID == 0x427 || PID == 0x436 || PID == 0x437
  #define SYS_MEM_ADDRESS 0x1FF00000 
  #define RAM_MEM_ADDRESS 0x20001000
#elif PID == 0x416
  #define SYS_MEM_ADDRESS 0x1FF00000 
  #define RAM_MEM_ADDRESS 0x20000800
#elif PID == 0x415
  #define SYS_MEM_ADDRESS 0x1FFF0000 
  #define RAM_MEM_ADDRESS 0x20003000
#else
  //undefined case
  #define SYS_MEM_ADDRESS 0x00000000 
  #define RAM_MEM_ADDRESS 0x00000000
#endif

/**
* @}
*/ 

/* Exported functions ------------------------------------------------------- */

/* Bootloader and USART configuration functions *******************************/
void bootloader_init(void);
void USART_IT_init(void);

#endif

/**
  * @}
  */

/**
  * @}
  */ 