/**
  ******************************************************************************
  * @file    bootloader.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   This file contains all the functions that ensure the proper working
  *           of USART interruptions and the bootloader mode.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "bootloader.h"
#include "stm32l1xx_usart.h"

/** @addtogroup STM32L1xx_StdPeriph_Driver
  * @{
  */

/** @defgroup BOOTLOADER
  * @brief BOOTLOADER Driver Modules
  * @{
  */ 

/* Private variables ---------------------------------------------------------*/
void (*sys_mem_bootjmp) (void);

/**
  * @brief  Initialisation de tout ce qui est nécessaire pour le fonctionement des interruptions levé par le port-série.
  * @note   Configuration des pins de GPIO pour le Rx et Tx de la communication et
  * @note   du USART IRQ. Cette fonction doit être appelée avant d'utiliser le programme
  * @param  None
  * @retval None
  */
void USART_IT_init(void) {
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
 
  //Initialisation des horloges
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
 
  //Liaison entre les pins et leur fonction
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
 
  /* Configure USART1 pins:  Rx and Tx ----------------------------*/
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 | GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
 
 
  /* Enable USART1 IRQ */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
 
  
  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);
 
  USART_Cmd(USART1,ENABLE);

  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}

/**
  * @brief  Fonction responsable du passage du stm32 en mode bootloader
  * @note   Le programme s'arrête complètement une fois que la fonction
  * @note   est appelée et ne peut être redémarré proporement qu'avec un reset.
  * @param  None
  * @retval None
  */
void bootloader_init (void )
{  
  //Fait le PC (Program Counter) pointer vers le SystemMemory reset vector (+4 offset)
  sys_mem_bootjmp = (void (*)(void)) (*((uint32_t *) (SYS_MEM_ADDRESS + 4) ));
  
  //Arrêt de toutes les tâches en exécution
  USART_DeInit(USART1);
  RCC_DeInit();
  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL = 0;
  
  //Désactive les interruptions
  __set_PRIMASK(1);
  
  //Attribue la valeur défaut au MSP
  __set_MSP(RAM_MEM_ADDRESS);
  
  //Fait le saut du PC
  sys_mem_bootjmp();
    
  
  while(1);
  //Ne dois jamais arriver ici
}

/**
  * @}
  */

/**
  * @}
  */
