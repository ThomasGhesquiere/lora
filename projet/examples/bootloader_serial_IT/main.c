/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple simple qui clignote un LED et permet le "flashement" d'un
  * autre programme .hex en utilisant le script python 
  * @note    Il y a deux LEDs configurés pour l'exemple, D2 et D3.
  * @note    Pour l'instant il faut mettre des jumper entre les pins X6.3 et X6.4
  * (LED 2) et entre les pins X6.5 et 6.6 (LED 3)
  ******************************************************************************
  */

//#include "oslmic.h"
#include "stm32l1xx_conf.h"
#include "debug.h"
#include "lib_adc.h"
#include "hw.h"
#define LED1_PIN 0
//#define LED1_PIN 1
#define LED_PORT GPIOA

static int cnt1 = 0;

osjob_t job1;

static void blink1(osjob_t* job);

static void blink1(osjob_t* job){
  hw_set_pin(LED_PORT, LED1_PIN, ++cnt1 & 1);
  os_setTimedCallback(job, os_getTime()+sec2osticks(1), blink1);
}

// toggle LED
static void initfunc (osjob_t* job) {
    hw_cfg_pin(LED_PORT, LED1_PIN, GPIOCFG_MODE_OUT | GPIOCFG_OSPEED_40MHz | GPIOCFG_OTYPE_PUPD | GPIOCFG_PUPD_PUP);
    os_setTimedCallback(&job1, os_getTime()+sec2osticks(1), blink1);
}    
    
    
int main(void)
{
  osjob_t initjob;
  
  os_init();
  debug_init();
  
  os_setCallback(&initjob, initfunc);
  
  os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

