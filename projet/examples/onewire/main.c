
#include "stm32l1xx_conf.h"
#include "lib_onewire.h"
#include "debug.h"
#include "lib_adc.h"


static void get_ds1820(osjob_t* job){
  debug_str("job \r\n");
  ds1820_write_data();
  os_setTimedCallback(job, os_getTime()+sec2osticks(1), get_ds1820);
}


int main(void)
{
    //struct_adc_t struct_adc;
    osjob_t initjob;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();
    
    os_setCallback(&initjob, get_ds1820);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

