/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du weather shield. Dans cet exemple, les donn�es du vent,
  * de pluie sont utilis�s. Les pins pin 14 et pin 12 du port B sont utilis�s.
  ******************************************************************************
  */

#include "stm32l1xx_conf.h"
#include "lib_weather_shield.h"
#include "debug.h"
#include "lib_adc.h"
#include "lib_adc_dma.h"
#include <stdbool.h>

u4_t temp = 1;
u4_t current_tic=0;
u4_t current_speed=0;
u4_t current_rain=0;

void wind_fct(osjob_t* j);
void rain_fct(osjob_t* j);
void wind_direction_fct(osjob_t* j);

void wind_fct(osjob_t* j) {
  u4_t last_tic = current_tic;
  current_tic = get_wind_tics();
  current_speed = ((current_tic-last_tic)/(3*temp))*2.4; //en tics/secondes)
  if(current_speed >= 1){
    debug_int(current_speed);
    debug_str(" km/h\r\n");
  }
  os_setTimedCallback(j, os_getTime()+sec2osticks(3*temp), wind_fct);
}

void rain_fct(osjob_t* j) {
  u4_t previous_rain = current_rain;
  current_rain = (get_rain_tics()); //en tics/secondes)
  if(current_rain != previous_rain){
    debug_int(current_rain);
    debug_str(" mm\r\n");
  }
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), rain_fct);
}

void wind_direction_fct(osjob_t* j) {
  debug_val("Direction : ",get_direction());
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), wind_direction_fct);
}


int main(void)
{
    //struct_adc_t struct_adc;
    osjob_t wind_job;
    osjob_t rain_job;
    osjob_t ligth_job;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();
    
    //On initialise le shield
    struct_weather_shield init_weather;
    init_weather.wind_enable = true;
    init_weather.wind_pin = 14;
    init_weather.wind_port = 1;
    init_weather.rain_enable = true;
    init_weather.rain_pin = 12;
    init_weather.rain_port = 1;
    
    init_weather.get_wind_direction_value = get_channel1;
    init_weather.delta = 0xF;
    
    
    debug_str("Begin init weather\r\n");
    init_weather_shield(init_weather);
    debug_str("End init weather\r\n");
    
    uint16_t reference[NUMBER_REFERENCE];
    
    reference[DIR_N] =  0xDF2;
    reference[DIR_NNW] = 0x0;
    reference[DIR_NW] = 0x0;
    reference[DIR_NWW] = 0x0;
    reference[DIR_W] = 0x0;
    reference[DIR_SWW] = 0x0;
    reference[DIR_SW] = 0xC7C;
    reference[DIR_SSW] =  0xD34;
    reference[DIR_S] = 0x3FC;
    reference[DIR_SEE] = 0x78C;
    reference[DIR_SE] = 0x833;
    reference[DIR_SEE] = 0x3FC;
    reference[DIR_E] = 0xBFD;
    reference[DIR_NEE] = 0x633;
    reference[DIR_NE] = 0x5B7;
    reference[DIR_NNE] = 0xA28;
    
    init_direction_reference(reference);
    debug_str("End init weather\r\n");
    //os_setTimedCallback(&wind_job_direction, os_getTime()+ms2osticks(50), wind_direction_fct);
    os_setTimedCallback(&rain_job, os_getTime()+ms2osticks(50), rain_fct);
    os_setTimedCallback(&wind_job, os_getTime()+ms2osticks(50), wind_fct);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif