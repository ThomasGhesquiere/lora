/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du capteur htu21d. Dans cette exemple, une 
  *          mesure de la temp�rature et de l'humidit� sont effectu�es toutes 
  *          les secondes. Ce test a �t� effectu� avec la platine weather shield
  *          de sparkfun.
  ******************************************************************************
  */

#include "stm32l1xx_conf.h"
#include "debug.h"
#include "lib_adc.h"
#include "lib_htu21d.h"

static void get_values(osjob_t* job){
  debug_str("\r\nHumidity : ");
  debug_int(get_humidity_htu21d());
  debug_str("\r\nTemperature : ");
  debug_int(get_temperature_htu21d());
  os_setTimedCallback(job, os_getTime()+sec2osticks(1), get_values);
}


static void init_htu21d_job(osjob_t* job){
  debug_str("init\r\n");
  htu21d_init();
  debug_str("init ok\r\n");
  os_setCallback(job, get_values);
}


int main(void)
{
    osjob_t initjob;
    os_init();
    debug_init();
    
    os_setCallback(&initjob, init_htu21d_job);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

