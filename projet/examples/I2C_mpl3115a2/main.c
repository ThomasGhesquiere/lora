/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du capteur mpl3115a2. Dans cette exemple, on 
  *          mesure l'altitude toute les secondes. Le test a �t� effectu� avec 
  *          la platine weather schield de sparfun.
  ******************************************************************************
  */


#include "stm32l1xx_conf.h"
#include "debug.h"
#include "lib_adc.h"
#include "lib_mpl3115a2.h"

static void get_values(osjob_t* job){
  debug_str("\r\n Altitude : ");
  debug_int(read_altitude());
  os_setTimedCallback(job, os_getTime()+sec2osticks(1), get_values);
}


static void init_mpl3115a2_job(osjob_t* job){
  debug_str("init\r\n");
  mpl3115a2_init(MODE_ALT, MODE_OS_4);
  //Permet d'ajouter un offset � l'altitude
  //set_offset(OFF_ALTITUDE, 40);
  debug_str("init ok\r\n");
  
  os_setCallback(job, get_values);
}


int main(void)
{
    //struct_adc_t struct_adc;
    osjob_t initjob;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();
    
    os_setCallback(&initjob, init_mpl3115a2_job);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

