/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du capteur DHT11. Permet d'envoyer sur le
  * le port s�rie la r�ponse du capteur ou une erreur �ventuelle.
  * @note    Changer la configuration des pins dans le fichier lib_dht11.h
  * @note    Pour l'instant il faut brancher le DHT11 sur GPIOB, Pin 12, i.e. 
  * WiMOD_Pad15, i.e. Pin X5.4 de la platine.
  ******************************************************************************
  */

#include "stm32l1xx_conf.h"
#include "debug.h"
#include "lib_dht11.h"
    
osjob_t job1;
u1_t dhtbuf[5];
u1_t read_status;

static void read(osjob_t* job);

static void read(osjob_t* job){
   read_status = read_DHT(dhtbuf);

  switch (read_status) {
    case BUS_BUSY : debug_str("Bus busy.\r\n");
                    break;
    case ERROR_NOT_PRESENT : debug_str("No sensor detected.\r\n");
                             break;
    case ERROR_ACK_TOO_LONG : debug_str("Acknowledge too long.\r\n");
                              break;
    case ERROR_SYNC_TIMEOUT : debug_str("Synchronisation error.\r\n");
                              break;
    case ERROR_DATA_TIMEOUT : debug_str("Data timeout.\r\n");
                              break;
    case ERROR_CHECKSUM : debug_str("Checksum error.\r\n");
                          break;
    case ERROR_NO_PATIENCE : debug_str("Wait two seconds between sensor requests\r\n");
                             break;
    default:  debug_str("Temp: ");
              debug_int(DHT11_Temperature(dhtbuf));
              debug_str(" Hum: ");
              debug_int(DHT11_Humidity(dhtbuf));
              debug_str("\r\n");
              break;           
  }
  
  // Wait a few seconds between measurements.
  os_setTimedCallback(job, os_getTime()+sec2osticks(2), read);
}

// toggle LED
static void initfunc (osjob_t* job) {
    os_setTimedCallback(&job1, os_getTime()+sec2osticks(1), read);
}  
    
    
int main(void)
{
    osjob_t initjob;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();

    os_setCallback(&initjob, initfunc);
  
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

