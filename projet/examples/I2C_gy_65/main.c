/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du capteur GY65. Permet d'envoyer sur le
  * le port série la réponse du capteur.
  * @note    La configuration des pins est particulier pour le I2C de la platine.
  * @note    Pour l'instant il faut brancher le SCL de GY65 sur GPIOB, Pin 8, i.e.
  * WiMOD_Pad21, i.e. Pin X7.4 de la platine et le SDA de GY65 sur GPIOB, Pin 9,
  * i.e. WiMOD_Pad23, i.e. Pin X7.6 de la platine.
  ******************************************************************************
  */

//#include "oslmic.h"
#include "stm32l1xx_conf.h"
#include "lib_gy_65.h"
#include "debug.h"
#include <stdio.h>
#include <math.h>

static void get_sensor(osjob_t* job){
  s8_t resultat=0;    
  debug_str("\n\r***********************************************************\n\r");

  resultat = receive_gy_65(MODE_TEMP);
  resultat = receive_gy_65(MODE_PRES_1); 
  debug_str("\r\npressure = ");
  resultat=calc_pressure(resultat,MODE_PRES_1);
  debug_int(resultat);
  debug_str("\r\ntemperature = ");
  debug_int(get_temp());
  debug_str("\r\n");

  os_setTimedCallback(job, os_getTime()+sec2osticks(1), get_sensor);
}

static void init_sensor(osjob_t* job){
  
  debug_str("Initialisation");
  gy_65_init();
  debug_str("Initialisation finie\n");
  os_setCallback(job, get_sensor);
}




int main(void)
{
    //struct_adc_t struct_adc;
    osjob_t initjob;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();
    
    os_setCallback(&initjob, init_sensor);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

