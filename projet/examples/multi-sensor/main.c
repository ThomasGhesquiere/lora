/**
******************************************************************************
* @file    main.c 
* @author  Project 4 - Smart Campus
* @version V1.0
* @date    08-Juin-2015
* @brief   Exemple d'utilisation du weather shield. Similaire � l'exemple weather_shield pour les capteurs digitaux.
* Les capteur I2C de la weather_shield sont impl�ment� (2 capteurs) et le capteur externe gy65 est rajout�
******************************************************************************
*/

#include "stm32l1xx_conf.h"
#include "lib_weather_shield.h"
#include "debug.h"
#include "lib_adc.h"
#include "lib_adc_dma.h"
#include "lib_gy_65.h"
#include "lib_htu21d.h"
#include "lib_mpl3115a2.h"
#include <stdbool.h>

u4_t temp = 2;
u4_t current_tic=0;
u4_t current_speed=0;
u4_t current_rain=0;

u4_t temp1;
u4_t temp2;
u4_t temp3;

u4_t wind;
u4_t rain;

void wind_fct(osjob_t* j) {
  u4_t last_tic = current_tic;
  current_tic = get_wind_tics();
  wind = ((current_tic-last_tic)/(temp))*2.4;
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), wind_fct);
}

void rain_fct(osjob_t* j) {
  u4_t previous_rain = current_rain;
  rain = (get_rain_tics()); 
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), rain_fct);
}


void gy65_fct(osjob_t* j) {
  receive_gy_65(MODE_PRES_1);
  temp1=get_temp();
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), gy65_fct);
}


void htud21_fct(osjob_t* j) { 
  temp2 = get_temperature_htu21d();
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), htud21_fct);
}

void mpl311_fct(osjob_t* j) { 
  temp3 = read_temperature();
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp), mpl311_fct);
}

void print_fct(osjob_t* j) { 
  debug_str(" ------------- WEATHER STATION -------------\r\n");
  debug_str(" - \r\n");
  debug_str(" - \r\n");
  debug_str(" - \r\n");
  debug_str(" - Temperature MPL : ");debug_int(temp1);debug_str(" -\r\n");
  debug_str(" - Temperature GY : ");debug_int(temp2);debug_str(" -\r\n");
  debug_str(" - Temperature HTU : ");debug_int(temp3);debug_str(" -\r\n");
  debug_str(" - \r\n");
  debug_str(" - Wind : ");debug_int(wind);debug_str("km/h -\r\n");
  debug_str(" - Rain : ");debug_int(rain);debug_str(" -\r\n");
  debug_str(" -----------------------------------------\r\n");
  os_setTimedCallback(j, os_getTime()+sec2osticks(temp),  print_fct);
}


int main(void)
{
    osjob_t wind_job;
    osjob_t rain_job;
    osjob_t gy65_job;
    osjob_t htud21_job;
    osjob_t mpl311_job;
    osjob_t print_job;
    os_init();
    
    debug_init();
    htu21d_init();
    mpl3115a2_init(MODE_TEMP, MODE_OS_1);
    gy_65_init();
    
    
    //On initialise le shield
    struct_weather_shield init_weather;
    init_weather.wind_enable = true;
    init_weather.wind_pin = 14;
    init_weather.wind_port = 1;
    init_weather.rain_enable = true;
    init_weather.rain_pin = 12;
    init_weather.rain_port = 1;
    
    
    init_weather_shield(init_weather);
    debug_str("End init weather\r\n");
    os_setTimedCallback(&rain_job, os_getTime()+ms2osticks(50), rain_fct);
    os_setTimedCallback(&wind_job, os_getTime()+ms2osticks(50), wind_fct);
    os_setTimedCallback(&gy65_job, os_getTime()+ms2osticks(50), gy65_fct);
    os_setTimedCallback(&htud21_job, os_getTime()+ms2osticks(50), htud21_fct);
    os_setTimedCallback(&mpl311_job, os_getTime()+ms2osticks(50), mpl311_fct);
    os_setTimedCallback(&print_job, os_getTime()+ms2osticks(50), print_fct);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif