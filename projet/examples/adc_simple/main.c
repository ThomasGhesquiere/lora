/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du watchdog pour un capteur analogique. Le capteur
  *          devra �tre branch� sur X6.6 de la platine.
  ******************************************************************************
  */

#include "stm32l1xx_conf.h"
#include "debug.h"
#include "lib_adc.h"

static void get_adc(osjob_t* job){
  debug_val("Val : ", get_adc_value(ADC1));
  os_setTimedCallback(job, os_getTime()+sec2osticks(1), get_adc);
}

void test_irq(){
  debug_str("appel irq ");
  debug_val("Val : ",  get_adc_value(ADC1));
}

static void init_adc_fct(osjob_t* job){
  init_irq_adc_handler(test_irq);
  init_adc(ADC_Channel_1,1,GPIOA, MODE_CONTINU, 0x250,0x050);
  debug_str("End init ADC\r\n");
  os_setCallback(job, get_adc);
}




int main(void)
{
    //struct_adc_t struct_adc;
    osjob_t initjob;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();
    //debug_str("End init\r\n");
    
   
    os_setCallback(&initjob, init_adc_fct);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

