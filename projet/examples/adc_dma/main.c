/**
  ******************************************************************************
  * @file    main.c 
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Exemple d'utilisation du dma avec 2 capteur analogiques diff�rents.
  *          Les pins � utiliser sont X6.6 pour la valeur1 et X5.18 pour la valeur2.
  *          Il y aura de nouvelle valeur toutes les secondes.
  ******************************************************************************
  */

#include "stm32l1xx_conf.h"
#include "debug.h"
#include "lib_adc_dma.h"

static void get_adc(osjob_t* job){
  debug_str("*************************\n\r");
  debug_val("Val1 : ", get_channel1());
  debug_val("Val2 : ", get_channel3());
  os_setTimedCallback(job, os_getTime()+sec2osticks(1), get_adc);
}

static void init_adc_fct(osjob_t* job){
  init_analog_input_dma();
  debug_str("End init ADC_dma\r\n");
  os_setCallback(job, get_adc);
}




int main(void)
{
    //struct_adc_t struct_adc;
    osjob_t initjob;
    // initialize runtime env
    os_init();
    // initialize debug library
    debug_init();
    //debug_str("End init\r\n");
    
   
    os_setCallback(&initjob, init_adc_fct);
    os_runloop();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

