# -*- coding: utf-8 -*-
import time
import serial
import commands
import argparse


#Le nom du port sur lequel est connecté la plateforme
PORT_NAME = "/dev/ttyUSB0"
#Le fichier à uploader
FILE ='default.hex'
#La vitesse
BAUD = 115200
ACK_BIT = b'\x79'
NACK_BIT = b'\x1F'

#Permet d'initialiser le port série
def init_port():
	ser = serial.Serial(
            port=PORT_NAME,
            baudrate=BAUD, 
            bytesize=8,      
            parity=serial.PARITY_EVEN,
            stopbits=1,
            xonxoff=0, 
            rtscts=0,
            timeout=5 
    )	
	return ser

#Permet d'envoyer la bonne séquence pour lancer le bootloader
def launch_bootloader(ser):
	ser.write("BOOT")

#Permet de trouver l'adresse du reset_handler (non implémentée car l'option -R de stm32flash fait très bien l'affaire)
def search_reset_location():
	return "0x00"

#Permet de parser la ligne de commande
def parse_command_line():
    parser = argparse.ArgumentParser(description = 'parser')
    parser.add_argument('-f', help = 'Permet de choisir le fichier à flasher (.hex).', metavar = 'fichier.hex', required = True)
    parser.add_argument('-p', help = 'Permet de choisir le port.', metavar = 'nom_du_port', required = True)
    args = parser.parse_args()
    return args


#############################
args = parse_command_line()
PORT_NAME = args.p
FILE = args.f
ser=init_port()
print("Port initialise")
launch_bootloader(ser)
print("Activation de la plateforme en mode bootloader")
ser.close()
time.sleep(2)
print(commands.getoutput('stm32flash -b 115200 -w '+ FILE +' -v -R ' + PORT_NAME))
