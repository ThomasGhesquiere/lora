/**
  ******************************************************************************
  * @file    lib_adc_dma.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion des convertions ADC pour deux channels.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_ADC_DMA_H__
#define __LIB_ADC_DMA_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_conf.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Analogical_Sensors
  * @{
  */

/** @addtogroup ADC_DMA_Sensors
  * @{
  */

/* Exported functions ------------------------------------------------------- */

/*Permet d'initialiser le DMA pour les ADCs des deux pins disponibles (GPIO_pin1 et GPIO_pin2)*/
void init_analog_input_dma();

/*Configure le DMA (appellée par init_analog_input)*/
void configure_DMA();

/*Configure les deux ADC (appellée par init_analog_input)*/
void configure_ADC();

/*Configure les GPIO (appellée par init_analog_input)*/
void configure_GPIO();

/*Permet d'obtenir la valeur de channel 1*/
uint16_t get_channel1();

/*Permet d'obtenir la valeur du channel 2*/
uint16_t get_channel3();

#endif

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

