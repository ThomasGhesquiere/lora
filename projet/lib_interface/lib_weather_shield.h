/**
  ******************************************************************************
  * @file    lib_weather_shield.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion de certains capteurs de la Sparkfun Weather Shield
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __LIB_WEATHER_SHIELD_H__
#define __LIB_WEATHER_SHIELD_H__

/* Includes ------------------------------------------------------------------*/

#include "lmic.h"
#include <stdbool.h>
#include <stdint.h>

/** @addtogroup Sensors_Drivers
 * @{
 */

/** @addtogroup Weather_Shield
  * @{
  */


/* Exported constants --------------------------------------------------------*/

#define NUMBER_REFERENCE 16

/**
* @brief Define servant pour les directions de la girouette
**/

#define DIR_N 0
#define DIR_NNW 1
#define DIR_NW 2
#define DIR_NWW 3
#define DIR_W 4
#define DIR_SWW 5
#define DIR_SW 6
#define DIR_SSW 7
#define DIR_S 8
#define DIR_SSE 9
#define DIR_SE 10
#define DIR_SEE 11
#define DIR_E 12
#define DIR_NEE 13
#define DIR_NE 14
#define DIR_NNE 15

/* Exported types ------------------------------------------------------------*/

/**
* @brief Structure de configuration de la plateforme weather_shield
**/
typedef struct{
        u1_t wind_port; /*!<  Le port d'entrée de la sortie wind_speed de la plateforme*/
        u1_t wind_pin; /*!<  Le pin d'entrée de la sortie wind_speed de la plateforme*/
        bool wind_enable; /*!< boolean d'activation de la mesure de la force du vent*/
        u1_t rain_port; /*!< Le port d'entrée de la sortie rain de la plateforme*/
        u1_t rain_pin; /*!<  Le pin d'entrée de la sortie rain de la plateforme*/
        bool rain_enable; /*!< boolean d'activation de la mesure de la quantité de pluie*/
        bool light_enable; /*!< boolean d'activation de la mesure de l'intensité lumineuse*/ 
        uint16_t (*get_light_value)(void); /*!< Pointeur sur la fonction à utiliser pour la récupération de l'entrée analogique de l'intensité lumineuse*/
        uint16_t (*get_wind_direction_value)(void); /*!< Pointeur sur la fonction à utiliser pour la récupération de la direction du vent*/
        uint16_t delta; /*!< Taille de l'intervalle pour la direction du vent (la direction sera au nord si : valeur_reference_nord - delta < valeur_mesurée < valeur_reference_nord + delta)*/
        
} struct_weather_shield;

/**
* @brief Structure des données enregistrées par le shield
**/
typedef struct{
        u4_t wind_tics; /*!< Le nombre de fermeture de l'interupteur de l'anémomètre*/
        u2_t wind_direction; /*!<  La direction du vent*/
        u4_t rain_tics; /*!<  Le nombre de fermeture de l'interupteur du pluviomètre */
} struct_weather_shield_datas;

/* Exported functions ------------------------------------------------------- */

/**
* @brief
* @retval None
**/
void increment_wind();

/**
* @brief
* @retval None
**/
void increment_rain();

/**
* @brief Permet d'initialiser la prise du vent (vitesse) et de la pluie
* @param init_struct la structure remplie d'initialisation
* @retval None
*/
void init_weather_shield(struct_weather_shield init_struct);

/**
* @brief Permet d'initiliser les références de voltage des directions de la girouette.
* @param tab_reference un tableau des 16 références correspondantes à 16 points cardinaux
* @retval None
* @note L'étallonage peut être compliqué à réaliser
**/
void init_direction_reference(uint16_t tab_reference[]);

/**
* @brief Permet de connaître le nombre de fermetures de l'anémomètre depuis l'initialisation
* @retval u4_t le nombre de fermeture de l'interrupteur de l'anémomètre
**/
u4_t get_wind_tics();

/**
* @brief Permet de remettre à zéro le compteur pour éviter un overflow (à utliser si l'application tourne depuis un certain temps)
**/
void reset_wind_tics();

/**
* @brief Permet de connaître le nombre de fermetures de l'interrupteur du pluviomètre
* @retval u4_t le nombre de fermetures de l'interrupteur du puviomètre
**/
u4_t get_rain_tics();

/**
* @brief Permet de remettre à zéro le compteur pour éviter un overflow (à utliser si l'application tourne depuis un certain temps)
**/
void reset_rain_tics();
/**
* @brief Permet d'obtenir l'intensité lumineuse
* @retval u2_t
**/
u2_t get_ligth();
/**
* @brief Permet d'obtenir la direction actuelle de la girouette. @ref init_direction_reference(uint16_t tab_reference[]) doit avoir été utilisée au préalable
* @retval u2_t une constante définie dans @ref lib_weather_shield.h indiquant la direction de la girouette
**/
u2_t get_direction();

/**
  * @}
  */

/**
  * @}
  */


#endif