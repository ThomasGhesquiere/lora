/**
  ******************************************************************************
  * @file    lib_adc_dma.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion des convertion ADC pour deux channels.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/ 
#include "lib_adc_dma.h"
#include "stm32l1xx.h"
#include "stm32l1xx_conf.h"
#include "debug.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Analogical_Sensors
  * @{
  */

/** @defgroup ADC_DMA_Sensors
  * @brief Interface entre LMIC et le biliothèque de STM32 (ADC et DMA)
  * @{
  */


/* Private variables ---------------------------------------------------------*/
__IO uint16_t converted_value[2]; /*!< Tableau servant au stockage des valeurs lues */


/*Permet d'initialiser les deux seules entrées disponibles sur la carte en analog*/
/**
* @brief Permet d'initialiser les deux seules entrées disponibles sur la carte (ADC_IN1 et ADC_IN3 ) en mode entrée analogique
* @retval None
**/
void init_analog_input_dma(){
  //on demande la configuration du DMA
  configure_DMA();
  //puis celle des GPIO
  configure_GPIO();
  //et enfin on active la conversion
  configure_ADC();

}

/**
* @brief Configuration du DMA pour les convertions dans le buffer
* @retval None
**/
void configure_DMA(){
  //Structure d'initialisation du DMA
  DMA_InitTypeDef DMA_InitStructure;
  
  //Horloge pour le DMA
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  //Valeurs par défaut du DMA
  DMA_DeInit(DMA1_Channel1);
  //Ici l'adresse du periph est 0x40012458 (trouvé dans les exemples de stm32)
  DMA_InitStructure.DMA_PeripheralBaseAddr = ((uint32_t)0x40012458);
  //La destination de la copie du DMA
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&converted_value;
  //DMA comme source
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  //Taille de la destination
  DMA_InitStructure.DMA_BufferSize = 2;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  //Pour mettre dans buf[0] puis buf[1]
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  //Taille de valeurs manipulées
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
  
  //Activation du DMA
  DMA_Cmd(DMA1_Channel1, ENABLE);
}

/**
* @brief Configuration des entrée ADC
* @retval None
**/
void configure_ADC(){
  //Structure d'initialisation de l'ADC
  ADC_InitTypeDef ADC_InitStructure;
  //Test pour savoir si l'horloge est prete
  while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);

  //Activation de l'horloge pour la conversion
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
  
  ADC_InitStructure.ADC_ScanConvMode = ENABLE;
  //En mode continu
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  //A la demande
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_Ext_IT11;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  //Alignement des données à droite
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  //Meilleure résolution permise par le processeur
  ADC_InitStructure.ADC_Resolution =  ADC_Resolution_12b;
  ADC_InitStructure.ADC_NbrOfConversion = 2;
  ADC_Init(ADC1, &ADC_InitStructure);

  //Configuration des deux channels demandés
  ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_4Cycles);
  ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 2, ADC_SampleTime_4Cycles);
  
  ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
  
  //Activation du DMA pour ADC1
  ADC_DMACmd(ADC1, ENABLE);
  
  //Acivation de ADC1
  ADC_Cmd(ADC1, ENABLE);

  //On attend que ADC1 soit pret
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET)
  {
  }

  //On peut lancer la première conversion
  ADC_SoftwareStartConv(ADC1);
  
}

/**
* @brief Configuration de la GPIO
* @retval None
**/
void configure_GPIO(){
  //Structure d'initialisation des GPIOs
  GPIO_InitTypeDef GPIO_InitStructure;
  
  RCC_HSICmd(ENABLE);

  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  //On choisi les deux pins disponibles en mode analogique
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
}

/**
* @brief Permet de lire la valeur convertie sur ADC_IN1
* @retval uint16_t La valeur convertie
* @note Récupération du buffer1
**/
uint16_t get_channel1(){
  return converted_value[0];
}


/**
* @brief Permet de lire la valeur convertie sur ADC_IN3
* @retval uint16_t La valeur convertie
* @note Récupération du buffer2
**/
uint16_t get_channel3(){
  return converted_value[1];
}

  
  
 
  
  
  
