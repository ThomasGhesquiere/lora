/**
  ******************************************************************************
  * @file    lib_gy_65.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothéque implémente toutes les fonctions nécéssaires à l'intégration d'un capteur gy_65
  * @note    Les 5 modes de ce capteur sont implémentés. Ils peuvent être séléctionnés via les macros.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_GY_65_H__
#define __LIB_GY_65_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_conf.h"
#include "oslmic.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/** @addtogroup GY_65 
  * @{
  */ 

/* Exported types ------------------------------------------------------------*/

/**
* @brief structure contenant les constantes pour la calibration et la température mesurée.
**/
typedef struct{ 
  short AC1; /*<    */
  short AC2; /*<    */
  short AC3; /*<    */
  unsigned short AC4; /*<    */
  unsigned short AC5; /*<    */ 
  unsigned short AC6; /*<    */ 
  short B1; /*<    */ 
  short B2; /*<    */ 
  short MB; /*<    */ 
  short MC; /*<    */ 
  short MD; /*<    */ 
  s8_t temp; /*!<  température mesurée. Nécéssaire pour le calcul de la pression  */ 
  s8_t B5; /*!<  constante calculée lors de la mesure de la température. Nécéssaire pour le calcul de la pression*/ 
} cal_gy_65_t;


/* Exported constants --------------------------------------------------------*/

/**
* @brief Adress I2C du capteur gy_65
**/
#define ADDRESS_GY_65 0xEE 

/** @defgroup Mode_Fonctionnement
  * @brief Le capteur gy_65 posséde un mode de fonctionnement pour la temperature uniquement et 4 modes pour la pression.
  *        Ces 4 modes sont plus ou moins précis et donc plus ou moins rapide. Pour le calcul de la pression, 
  *        la température doit être calculée précédemment. 
  * @{
  */
#define MODE_PRES_0 0  /*!< mode de calcul de la pression le plus rapide */
#define MODE_PRES_1 1   /*!< mode de calcul de pression */
#define MODE_PRES_2 2 /*!< mode de calcul de pression */
#define MODE_PRES_3 3 /*!< mode de calcul de pression le plus précis */
#define MODE_TEMP 4 /*!< mode de calcul de la température uniquement */
 /**
* @}
*/

/**
* @defgroup Valeur_Commande
* @brief Valeur à écrire dans le registre 0xF4 pour demander le calcul de données dans le mode choisi
* @{
*/
#define CTRL_REG_PRES_0 0x34  /*!< à écrire dans le registre 0xF4 du capteur */
#define CTRL_REG_PRES_1 0x74 /*!< à écrire dans le registre 0xF4 du capteur */
#define CTRL_REG_PRES_2 0xB4 /*!< à écrire dans le registre 0xF4 du capteur */
#define CTRL_REG_PRES_3 0xF4 /*!< à écrire dans le registre 0xF4 du capteur */
#define CTRL_REG_TEMP 0x2E /*!< à écrire dans le registre 0xF4 du capteur */
/**
* @}
**/


/**
* @defgroup Temps_Convertion
* @brief Temps de convertion analogique/numérique pour le mode choisi. Ce temps est en ms et est arrondi à l'entier supérieur.
* @{
**/
#define TIME_CONV_TEMP 5 /*!< temps à attendre avant la fin de la convertion */
#define TIME_CONV_PRES_0 5 /*!< temps à attendre avant la fin de la convertion */
#define TIME_CONV_PRES_1 8 /*!< temps à attendre avant la fin de la convertion */
#define TIME_CONV_PRES_2 14 /*!< temps à attendre avant la fin de la convertion */
#define TIME_CONV_PRES_3 26 /*!< temps à attendre avant la fin de la convertion */
/**
* @}
**/

/**
* @brief Vitesse du i2c à 50kHz
**/
#define I2C_SPEED 50000


/* Exported functions ------------------------------------------------------- */
void gy_65_init(void);
s8_t receive_gy_65(u1_t WORKING_MODE);
void calibration_gy_65(void);
s8_t calc_temp (s8_t UT);
s8_t calc_pressure (s8_t UP,u1_t WORKING_MODE);
s8_t get_temp(void);

#endif


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
