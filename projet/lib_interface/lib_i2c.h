/**
  ******************************************************************************
  * @file    lib_i2c.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Ce fichier contient toutes les fonctions necessaires pour l'utilisation de capteur i2c
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_i2c_H__
#define __LIB_i2c_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx.h"
#include "stm32l1xx_conf.h"
#include "stm32l1xx_gpio.h"
#include "oslmic.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/* Exported functions ------------------------------------------------------- */
void i2c_init_pin(I2C_TypeDef* I2CX, GPIOSpeed_TypeDef GPIO_speed, uint16_t I2C_own_address1, uint16_t I2C_AcknowledgedAddress, uint32_t I2C_speed);
void i2c_write(I2C_TypeDef* I2Cx,u2_t chip_address, u2_t* tab_data, u2_t nb_data);
void i2c_start_read(I2C_TypeDef* I2Cx,u2_t chip_address, u2_t register_address);
void i2c_restart_read(I2C_TypeDef* I2Cx,u2_t chip_address, u2_t* tab_data,u2_t nb_data);

#endif

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */