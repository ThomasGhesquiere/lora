/**
  ******************************************************************************
  * @file    lib_interupt.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion des interruption des pins digitaux. Attention EXTI_IRQHandler doit être définie dans le projet et dans le Makefile (voir l'exemple)
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/ 
#include "lmic.h"
#include "hw.h"
#include "debug.h"
#include "stdint.h"
#include "lib_interupt.h"
#include <stdbool.h>

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @defgroup Digital_Interrupt
  * @{
  */


/** 
* Tableau de pointeurs de fonction, sert à stocker les fonctions appellées lors de la detection des interruptions sur des pins
*/
void (*fct_irq_tab[16])(void);

/** 
* Tableau de boolean, fct_is_present[i] true si on écoute effectivement sur le pin i
*/
bool fct_is_present[16]; /*!<  indique si une fonction écoute sur le pin*/
u4_t last_call[16]; /*!<  heure du dernier appel de la fonction (utilisé pour le debounce) */
u4_t debounce_intervalle[16]; /*!<  Intervalle demandé par l'utilisateur pour le debounce */

/**
* @brief Permet de choisir une fonction à appeler lors de la détéction d'une interruption
* @param pin_name le numéro de pin
* @param irq_fct un pointeur de fonction sur la fonction appelée lors de l'interruption sur le pin
* @retval None 
**/
void set_pin_irq(u1_t pin_name, u4_t debounce, void (*irq_fct)(void)){
  fct_irq_tab[pin_name] = irq_fct;
  fct_is_present[pin_name] = true;
  last_call[pin_name] = os_getTime();
  debounce_intervalle[pin_name] = debounce;
}


/**
* @brief Fonction appellée lors d'une interruption quelconque, celle-ci associe le pin résponsable de l'interruption et la fonction a appeler
* @retval None
* @note Cette fonction doit être définie dans la paramètres du projet, comme fonction liée aux interruptions (voir dossier /examples pour plus d'informations)
**/
void sensorirq(){
    u4_t EXTI_PR = EXTI->PR;
    u4_t EXTI_PR_OLD = EXTI_PR;
    u1_t i;
    //on va parcourir l'ensemble des pins possibles
    for(i=0; i<16; i++){
      //si une fonction écoute sur un pin et qu'une interruption s'est produite dessus
      if(fct_is_present[i] && (EXTI_PR & (1<<(i))) != 0){
        if(os_getTime() - last_call[i] > ms2osticks(debounce_intervalle[i])){
          (*(fct_irq_tab[i]))();
          last_call[i] = os_getTime();
        }
        EXTI->PR = (1<<(i)) | EXTI->PR;
      }
      EXTI_PR = EXTI->PR;
    }
}

