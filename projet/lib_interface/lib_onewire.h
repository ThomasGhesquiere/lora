/**
  ******************************************************************************
  * @file    lib_onewire.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion du capteur ds1820.
  * @note    Dans cette biliothèque, seul le reset marche. Il n'est pas possible d'obtenir des valeurs
  * du ds1820. Attention, les délais utilisés doivent être assez précis pour générer des impulsions de l'ordre de la mircoseconde
  ******************************************************************************
  */

#ifndef _LIB_ONEWIRE_H
#define _LIB_ONEWIRE_H

#define MAX 100
#define TSLOT 80
#define TREC 2
#define TLOW0 80
#define TLOW1 7


void set_in_ds1820();

void set_out_ds1820();

u4_t ds1820_read_data();

void generate_low_pulse();

void generate_high_pulse();

void write(u1_t value);

void reset();

bool read_bit();

u1_t read_Byte();

void ds1820_write_data();


#endif
