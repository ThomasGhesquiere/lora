/**
  ******************************************************************************
  * @file    lib_i2c.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Ce fichier implémente les fonctions permettant l'utilisation du protocole i2c:
    - initialisation et configuration de l'interface
    - Envoie de données
    - reception de données
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/  
#include "lib_i2c.h"
#include "stm32l1xx_i2c.h"


/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @defgroup I2C_Protocol
  * @{
  */


/**
* @brief  Initialise le GPIO, le peripherique I2C et la clock.
* @note   Les pins 8 et 9 seront utilisés respectivement pour l'horloge et les données bus I2C.
* @param  I2Cx: Ou x peut etre 1 ou 2 pour selectionner le peripherique I2C.
* @param GPIO_speed: Permet de specifier la vitesse de sortie des pins 8 et 9. Ce paramètre est une valeur de @ref GPIOSpeed_TypeDef.	
* @param  I2C_own_address1: Précise la première adresse du péripherique. Elle peut etre sur 8 ou 10 bits.
* @param  I2C_AcknowledgedAddress: Précise si le periphérique fonctionne sur 8 ou 10 bits.
* @param I2C_speed: Fréquence de l'I2C. Inferieur à 400kHz.
* @retval None
*/
void i2c_init_pin(I2C_TypeDef* I2Cx, GPIOSpeed_TypeDef GPIO_speed, uint16_t I2C_own_address1, uint16_t I2C_AcknowledgedAddress, uint32_t I2C_speed ){
   GPIO_InitTypeDef GPIO_InitStructure;
   I2C_InitTypeDef I2C_InitStructure;
  /*enable I2C*/
    I2C_Cmd(I2Cx,ENABLE);
    
    /* I2CX SDA and SCL configuration */ 
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9; //
    GPIO_InitStructure.GPIO_Speed = GPIO_speed;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure); //
    
    /* I2CX clock enable */
    if(I2Cx == I2C1){
      /* I2C1 clock enable */
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
      /*SCL is pin1 and SDA is pin2 for I2C1*/
      GPIO_PinAFConfig(GPIOB,GPIO_PinSource8 ,GPIO_AF_I2C1);
      GPIO_PinAFConfig(GPIOB,GPIO_PinSource9,GPIO_AF_I2C1);
    }else{
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);
      /*SCL is pin1 and SDA is pin2 for I2C2*/
      GPIO_PinAFConfig(GPIOB,GPIO_PinSource8 ,GPIO_AF_I2C2);
      GPIO_PinAFConfig(GPIOB,GPIO_PinSource9,GPIO_AF_I2C2);
    }
    
    
        
    /* I2CX configuration */
    I2C_StructInit(&I2C_InitStructure);//useless
    I2C_DeInit(I2Cx);//useless
    
    I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
    I2C_InitStructure.I2C_OwnAddress1 = I2C_own_address1; //0x00
    I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress; 
    I2C_InitStructure.I2C_ClockSpeed = I2C_speed;
    I2C_ITConfig(I2Cx, I2C_IT_ERR, ENABLE);
    I2C_Init(I2Cx, &I2C_InitStructure);
  
}

/**
* @brief Envoie sur le bus, à destination du bon esclave, les données du tableau.
* @note Les données sont envoyées dans l'ordre du tableau.
* @param I2C: Où x peut etre 1 ou 2 pour selectionner le peripherique I2C.
* @chip chip_address: Adresse du périphérique.
* @param tab_data: Tableau des données à envoyer. Celui-ci doit avoir été alloué au préalable.
* @param nb_data: Nombre de données à envoyer. Doit être inférieur ou égal à la taille du tableau
* @retval None
*/
void i2c_write(I2C_TypeDef* I2Cx,u2_t chip_address, u2_t* tab_data, u2_t nb_data){
      while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
    /* initiate start sequence */
    I2C_GenerateSTART(I2Cx, ENABLE);
    /* check start bit flag */    
    while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB));
    
    /*send write command to chip*/
    I2C_Send7bitAddress(I2Cx, chip_address, I2C_Direction_Transmitter);
    /*check master is now in Tx mode*/
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  
    /*control register*/
    for (u2_t i=0; i< nb_data; i++){
      I2C_SendData(I2Cx, tab_data[i]);
      while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    }
    
    /*wait for byte send to complete*/
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    
    /*generate stop*/
    I2C_GenerateSTOP(I2Cx, ENABLE);
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));
}

/**
* @brief Initialise une lecture. Envoie sur le bus une donnée pour le périphérique séléctionné. Ne ferme pas la connection.
* @note Doit être suivie de @ref i2c_restart_read
* @param I2Cx: Où x peut etre 1 ou 2 pour selectionner le peripherique I2C.
* @param chip_address: Adresse du périphérique.
* @param: data: Données envoyées en écriture au périphérique. Cette donnée sera l'adresse du registre de début de lecture lors de l'appel de @ref i2c_restart_read.
* @retval None
*/
 void i2c_start_read(I2C_TypeDef* I2Cx,u2_t chip_address, u2_t data){
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
    //re-enable ACK bit incase it was disabled last call
    I2C_AcknowledgeConfig(I2Cx, ENABLE);
    // Test on BUSY Flag 
    while (I2C_GetFlagStatus(I2Cx,I2C_FLAG_BUSY));
    // Enable the I2C peripheral 
    I2C_GenerateSTART(I2Cx, ENABLE);
    //	Test on start flag 
    while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_SB));
    
    // Send device address for write 
    I2C_Send7bitAddress(I2Cx, chip_address, I2C_Direction_Transmitter);
    // Test on master Flag 
    while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
    
    // Send the device's internal address to write to 
    I2C_SendData(I2Cx, data);
    // Test on TXE FLag (data sent) 
    while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_TXE));
 }
 
/**
* @brief Effectue une lecture. Reçoit les données demandées au périphérique séléctionné. Ferme la connection.
* @note Doit être précédée de @ref i2c_start_read. Les données sont lues à partir de l'adresse du registre demandée avec @ref i2c_start_read.
* @param I2Cx: Où x peut etre 1 ou 2 pour selectionner le peripherique I2C.
* @param chip_address: Adresse du périphérique.
* @param tab_data: Tableau qui contiendra les données demandées. Celui-ci doit être alloué au préalable.
* @param: nb_data: Nombre de données demandées.
* @retval None
*/
 void i2c_restart_read(I2C_TypeDef* I2Cx,u2_t chip_address, u2_t* tab_data, u2_t nb_data){
    //On réalise un nouveau start
    I2C_GenerateSTART(I2Cx, ENABLE);
    //
    while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_SB));
    
    //Le stm32 passe en mode emetteur
    I2C_Send7bitAddress(I2Cx, chip_address , I2C_Direction_Receiver);
    while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
    
	//On va lire toutes les données qui doivent arriver
    for(u2_t i=0; i< nb_data; i++){
        while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));
        tab_data[i]= I2C_ReceiveData(I2Cx);
    }
   
    // Pour envoyer le NACK final
    I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
    I2C_AcknowledgeConfig(I2Cx, DISABLE);
    
    // Permet d'envoyer le STOP sur le bus 
    I2C_GenerateSTOP(I2Cx, ENABLE);
	//Test de la bonne execution du stop
    while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF));
 }

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
