/**
  ******************************************************************************
  * @file    lib_gy_65.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Implémentation des fonctions nécassaires à l'utilisation du capteur gy_65
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/  
#include "lib_gy_65.h"
#include "lib_i2c.h"
#include "delay.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/** @defgroup GY_65 
  * @brief GY_65 driver modules
  * @{
  */ 

/* Private variables ---------------------------------------------------------*/
cal_gy_65_t cal_gy_65;


/**
* @brief Cette fonction permet de paramétrer les pins, GPIO et I2C. 
*        Cette fonction appelle aussi la fonction de calibration du capteur.
* @retval None
**/
void gy_65_init(void)
{
    i2c_init_pin(I2C1, GPIO_Speed_400KHz , 0, I2C_AcknowledgedAddress_7bit, 50000 );
    calibration_gy_65();
}

/**
* @brief Cette fonction envoie au capteur une commande pour la mesure et la conversion en numérique d'une valeur en fonction du mode désiré.
*        Cette fonction effectue aussi l'attente nécessaire à cette convertion.
* @param WORKING_MODE mode de fonctionnement choisi
* @retval None
**/
void gy_65_cmd(u1_t WORKING_MODE)
{
  u2_t tab[2];
  tab[0]=0xF4;
  switch (WORKING_MODE) {
      case MODE_PRES_0: tab[1]=CTRL_REG_PRES_0;
                        i2c_write(I2C1,ADDRESS_GY_65, tab,2);
                        os_delay(TIME_CONV_TEMP);
                         break;
      case MODE_PRES_1:  tab[1]=CTRL_REG_PRES_1;
                        i2c_write(I2C1,ADDRESS_GY_65, tab,2);
                        os_delay(TIME_CONV_PRES_0);
                         break;
      case MODE_PRES_2:  tab[1]=CTRL_REG_PRES_2;
                        i2c_write(I2C1,ADDRESS_GY_65, tab,2);
                        os_delay(TIME_CONV_PRES_2);
                         break;
      case MODE_PRES_3:  tab[1]=CTRL_REG_PRES_3;
                        i2c_write(I2C1,ADDRESS_GY_65, tab,2);
                        os_delay(TIME_CONV_PRES_3);
                         break;
      default:  tab[1]=CTRL_REG_TEMP;
                i2c_write(I2C1,ADDRESS_GY_65, tab,2);
               os_delay(TIME_CONV_TEMP);
               break;
    }
}

/**
* @brief Réalise la demande de convertion et permet de lire la valeur mesurée par le capteur. Lors du calcul de la pression,
*        il faut avant tout faire la mesure de la température.
* @param WORKING_MODE mode de fonctionnement désiré
* @retval s8_t valeur réélle de la mesure.
**/
s8_t receive_gy_65(u1_t WORKING_MODE)
{    
    if (WORKING_MODE != MODE_TEMP) {
      
      // mesure de la température
      u2_t tab[3]={0,0,0};
      receive_gy_65(MODE_TEMP);
      
      //mesure de la pression
      // envoie de la commande au capteur
      gy_65_cmd(WORKING_MODE);
      // initialisation de la lecture
      i2c_start_read(I2C1,ADDRESS_GY_65, 0xF6);
      // lecture du resultat
      i2c_restart_read(I2C1,ADDRESS_GY_65,tab,3);
      return calc_pressure((tab[0]<<(8 + WORKING_MODE)) + (tab[1]<< WORKING_MODE)+ (tab[2]>>(8-WORKING_MODE)),WORKING_MODE);
    }
    else {
      u2_t tab[2];
      // envoie de la commande au capteur
      gy_65_cmd(MODE_TEMP);
      // initialisation de la lecture
      i2c_start_read(I2C1,ADDRESS_GY_65, 0xF6);
      // lecture du resultat
      i2c_restart_read(I2C1,ADDRESS_GY_65,tab,3);
      // enregistrement de la température réel dans la structure
      cal_gy_65.temp=calc_temp((tab[0] << 8) + tab[1]);
      return cal_gy_65.temp;
    }
    
}

/**
* @brief Effectue la calibration du capteur. Demande la lecture de 11 paramètres et les stoques dans la structure @ref cal_gy_65_t
* @retval None
**/
void calibration_gy_65(void){
  u2_t tab[22];
    i2c_start_read(I2C1,ADDRESS_GY_65,0xAA);
    i2c_restart_read(I2C1,ADDRESS_GY_65,tab,22);
    
    cal_gy_65.AC1=(tab[0] << 8) + tab[1];
    cal_gy_65.AC2=(tab[2] << 8) + tab[3];
    cal_gy_65.AC3=(tab[4] << 8) + tab[5];
    cal_gy_65.AC4=(tab[6] << 8) + tab[7];
    cal_gy_65.AC5=(tab[8] << 8) + tab[9];
    cal_gy_65.AC6=(tab[10] << 8) + tab[11];
    cal_gy_65.B1=(tab[12] << 8) + tab[13];
    cal_gy_65.B2=(tab[14] << 8) + tab[15];
    cal_gy_65.MB=(tab[16] << 8) + tab[17];
    cal_gy_65.MC=(tab[18] << 8) + tab[19];
    cal_gy_65.MD=(tab[20] << 8) + tab[21];
    cal_gy_65.temp=0;
}  

/**
* @brief Calcule la valeur réelle de la température grâce aux constantes mesurées lors de la callibration
* @param UT température directement mesurer par le capteur
* @retval s8_t température réelle ajustée
**/
s8_t calc_temp (s8_t UT) {
  s8_t X1, X2, B5, T;
  X1 = ((UT - cal_gy_65.AC6) * cal_gy_65.AC5) / 32768;
  X2 = (cal_gy_65.MC * 2048) / (X1 + cal_gy_65.MD);
  B5 = X1 + X2;
  cal_gy_65.B5 = B5; // la constante B5 est stockée dans la strucutre car elle peut servir pour le calcul de la pression 
  T = (B5 + 8) / 16;
  return T;
}

/**
* @brief Permet l'accès à la température stoquée dans la structure @ref cal_gy_65_t
* @retval s8_t 
**/
s8_t get_temp(){
    return cal_gy_65.temp;
}

/**
* @brief Calcule la valeur réélle de la pression en fonction des paramètres de la calibration
* @param UP valeur de la pression mesurée directement par le capteur
* @param WORKING_MODE mode de fonctionnement choisi
* @retval s8_t valeur réelle de la pression ajustée
**/
s8_t calc_pressure (s8_t UP,u1_t WORKING_MODE) {
  s8_t B6, X1=0, X2=0, X3=0, B3=0, B4=0, B7=0, p=0 ;
  
  B6 = cal_gy_65.B5 - 4000;
  X1 = cal_gy_65.B2 * (((B6 * B6) >> 12) >> 11);
  X2 = (cal_gy_65.AC2 * B6) >> 12;
  X3 = X1 + X2; 
  B3 = (((cal_gy_65.AC1*4 + X3) << WORKING_MODE) + 2) >> 2;
  X1 = (cal_gy_65.AC3*B6) >> 13;
  X2 = (cal_gy_65.B1 * ((B6 * B6) >> 12 )) >> 16;
  X3 = ((X1 + X2) + 2) >> 2;
  B4 = (cal_gy_65.AC4 * (u8_t)(X3 + 32768)) >> 15;
  B7 = ((u8_t)(UP) - B3) * (50000 >> WORKING_MODE); 
  if (B7 < 0x40000000) { 
    p = (B7 *2) / B4;  
  }else{ 
      p = (B7 / B4) * 2;
  }
  X1 = (p >> 8) * (p >> 8);
  X1 = (X1 * 3038) >> 16;
  X2 = (-7357 * p) >> 16;
  p = p + ((X1 + X2 + 3791) >> 4);
  return p;
 
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
