/**
  ******************************************************************************
  * @file    lib_adc.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion des convertion ADCs
  * @note Avec cette méthode, il n'est pas possible d'utiliser les deux entrées analogique de la plateforme.
  * @note Pour réaliser deux convertions ADC sur deux pins distincts, il faut utiliser la bibliothèque @ref lib_adc_dma.h 
  ******************************************************************************
  */

/*
 * Bibliothèque d'interface entre lmic et les librairies de biliothèques stm32
 * Facilite l'utilisation des entrées Analogique
 */

/* Includes ------------------------------------------------------------------*/ 
#include "lib_adc.h"
#include "stm32l1xx.h"
#include "stm32l1xx_gpio.h"
#include "debug.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Analogical_Sensors
  * @{
  */

/** @defgroup ADC_Sensors
  * @brief Interface entre LMIC et le biliothèque de STM32 (ADC)
  * @{
  */


/* Private variables ---------------------------------------------------------*/
struct_adc_t struct_adc;



/**
 * @brief Fonction d'initialisation d'un pin ADC
 * @param ADC_Channel Le channel désiré
 * @param GPIO_Pin Le pin désiré
 * @param GPIOX La port GPIO
 * @param mode Peut valoir @ref MODE_CONTINU ou @ref MODE_ASK
 * @param HighThreshold Le seuil haut
 * @param LowThreshold Le seuil bas
 * @return void
 * @note Dans le cas de la plateforme étudiée, ADC_Channel peut valoir ADC_Channel1 ou ADC_Channel3.
 * GPIO_Pin peut valoir GPIO_Pin_1 ou GPIO_Pin_3.
 * @note Dans le cas du mode @ref MODE_ASK, les valeurs de threshold sont facultatives
 * @note Dans le cas du mode @ref MODE_CONTINU, la fonction @ref init_irq_adc_handler doit être utilisée
 */
void init_adc(uint8_t ADC_Channel, uint32_t GPIO_Pin, GPIO_TypeDef* GPIOX, u1_t mode, uint16_t HighThreshold, uint16_t LowThreshold){
    // initialisation de la structure
    struct_adc.GPIO_Pin= GPIO_Pin;
    struct_adc.GPIOX=GPIOA;
    struct_adc.ADC_Channel=ADC_Channel;
    struct_adc.mode = mode;
    struct_threshold threshold;
    threshold.highThreshold = HighThreshold;
    threshold.lowThreshold = LowThreshold;
    struct_adc.threshold = threshold;
    
    //Initialisation des GPIO
    init_adc_GPIO(&(struct_adc.GPIO_InitStructure), struct_adc.GPIO_Pin, struct_adc.GPIOX);
    
    //Initialisation de l'horloge
    init_adc_RCC();
    
    //Configuration du ADC
    init_adc_config(&(struct_adc.ADC_InitStructure), struct_adc.ADC_Channel, mode,threshold);
    
    //Si le mode est continu, activer les interruptions de fin de convertion
    if(mode == MODE_CONTINU){
      init_adc_NVIC(&(struct_adc.NVIC_InitStructure));
    }
   
}

/**
* @brief Initialisation du GPIO pour le pin séléctionné
* @param GPIO_InitStruct La structure d'initialisation
* @param GPIO_Pin Le pin désiré
* @param GPIOX Le port désiré
*/
void init_adc_GPIO(GPIO_InitTypeDef* GPIO_InitStruct, uint32_t GPIO_Pin, GPIO_TypeDef* GPIOX){
	

  GPIO_InitStruct->GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStruct->GPIO_PuPd  = GPIO_PuPd_NOPULL;

  //Active l'horloge
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  //Permet de choisir le bon pin
  GPIO_InitStruct->GPIO_Pin = GPIO_Pin;
  GPIO_Init(GPIOX, GPIO_InitStruct); 
}

/**
* @brief Initialisation de la clock
*/
void init_adc_RCC(){
	/* Enable the HSI */
  RCC_HSICmd(ENABLE);
  /* Wait until HSI oscillator is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET);

  /* Enable ADC1 clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
}

/**
* @brief Initialisation de la configuration des ADCs
* @param ADC_InitStruct Structure d'initialisation 
* @param ADC_Channel Le channel de l'ADC
* @param mode Le mode de convertion
* @param threshold Pour fixer les seuils (en mode @ref MODE_CONTINU)
*/
void init_adc_config(ADC_InitTypeDef* ADC_InitStruct, uint8_t ADC_Channel,u1_t mode, struct_threshold threshold){
  //Remise à zéro
  ADC_StructInit(ADC_InitStruct);
  ADC_InitStruct->ADC_Resolution = ADC_Resolution_12b;
  
  //Si le mode est continu
  if(mode == MODE_CONTINU){
      ADC_InitStruct->ADC_ScanConvMode = ENABLE;
      ADC_InitStruct->ADC_ContinuousConvMode = ENABLE;
  }
  else{
      ADC_InitStruct->ADC_ScanConvMode = DISABLE;
      ADC_InitStruct->ADC_ContinuousConvMode = DISABLE;
  }

  ADC_InitStruct->ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStruct->ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStruct->ADC_NbrOfConversion = 1;
  ADC_Init(ADC1, ADC_InitStruct);

  //Choix du bon channel
  ADC_RegularChannelConfig(ADC1, ADC_Channel, 1, ADC_SampleTime_16Cycles);

  //Si le mode est continu, on peut fixer le watchdog pour les interruptions
  if(mode == MODE_CONTINU){
    //Configuration des seuils haut et bas des threshold
    ADC_AnalogWatchdogThresholdsConfig(ADC1, threshold.highThreshold, threshold.lowThreshold);
    ADC_AnalogWatchdogSingleChannelConfig(ADC1, ADC_Channel);
    ADC_AnalogWatchdogCmd(ADC1, ADC_AnalogWatchdog_SingleRegEnable);
  }
  else{ //Sinon on peut lancer la convertion
     ADC_Cmd(ADC1, ENABLE);
     ADC_SoftwareStartConv(ADC1);
  }
}

/**
 * @brief Initialisation des interruptions pour le mode @ref MODE_CONTINU
 * @param NVIC_InitStructure La structure d'initialisation
 */
void init_adc_NVIC(NVIC_InitTypeDef* NVIC_InitStructure){
  //Configuration des interruptions avec une faible priorité
  NVIC_InitStructure->NVIC_IRQChannel = ADC1_IRQn;
  NVIC_InitStructure->NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure->NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure->NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(NVIC_InitStructure);
  
  //Activation
  ADC_ITConfig(ADC1, ADC_IT_AWD, ENABLE);
 
  //Activation de l'ADC
  ADC_Cmd(ADC1, ENABLE);

  //On attend l'activation
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET);
  //On commence la convertion
  ADC_SoftwareStartConv(ADC1);
  //debug_val("fonct ptr = ", ((u4_t)(*struct_adc.irq_function)));
}

/**
 * @brief Permet de changer les seuils du watchdog
 * @param HighThreshold Le seuil haut
 * @param LowThreshold Le seuil bas
 */
void set_treshold (uint16_t HighThreshold , uint16_t LowThreshold){
  struct_adc.threshold.highThreshold = HighThreshold;
  struct_adc.threshold.lowThreshold = LowThreshold;
  ADC_AnalogWatchdogThresholdsConfig(ADC1, HighThreshold, LowThreshold);
}

/**
 * @brief Permet de changer le seuil supérieur du watchdog
 * @param HighThreshold Le seuil haut
 */
void set_threshold_high(uint16_t HighThreshold){
  struct_adc.threshold.highThreshold = HighThreshold;
  ADC_AnalogWatchdogThresholdsConfig(ADC1, HighThreshold, struct_adc.threshold.lowThreshold);
}

/**
 * @brief Permet de changer le seuil inférieur du watchdog
 * @param LowThreshold Le seuil bas
 */
void set_threshold_low(uint16_t LowThreshold){
  struct_adc.threshold.lowThreshold = LowThreshold;
  ADC_AnalogWatchdogThresholdsConfig(ADC1, struct_adc.threshold.highThreshold, LowThreshold);
}

/**
* @brief Permet de récupérer une valeur convertie (en mode continu ou sur demande)
* @param ADCx Le convertisseur choisi
*/
uint16_t get_adc_value(ADC_TypeDef* ADCx){
  if(struct_adc.mode == MODE_ASK){
     ADC_Cmd(ADCx, ENABLE);
     ADC_SoftwareStartConv(ADCx);
  }
  return ADC_GetConversionValue(ADCx);
}

/**
* @brief Fonction utilisée lors d'une fin de convertion en @ref Mode_Convertion CONTINU
* @param pfunction Un pointeur sur la fonction a appeler lors de la fin de la convertion
* @note Si une fin de convertion est détéctée, la fonction pfunction sera automatiquement appelée
*/
void init_irq_adc_handler(ptr_fonct pfunction){
    struct_adc.irq_function = pfunction;
}

/**
* @brief Appel le handler des interruptions en @ref Mode_Convertion CONTINU
*/
void irq_adc_handler(void){
    (*struct_adc.irq_function)();
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
