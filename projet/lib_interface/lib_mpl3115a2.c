/**
  ******************************************************************************
  * @file    lib_mpl3115a2.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion du capteur I²C mpl3115a2
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "lib_mpl3115a2.h"
#include "lib_i2c.h"
#include "oslmic.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/** @defgroup MPL3115A2
  * @brief MPL3115A2 driver modules
  * @{
  */ 


/**
* @brief Initialise la mesure du capteur mpl3115a2. Cette fonction doit être appelée une unique fois avant de prendre les mesures 
* @param measure_mode valeur à mesurer @ref Mode_mesure
* @param precision_mode @ref Mode_OS_X
* @retval None
**/
void mpl3115a2_init(u1_t measure_mode, u1_t precision_mode)
{
  i2c_init_pin(I2C1, GPIO_Speed_40MHz, 0, I2C_AcknowledgedAddress_7bit, 50000);
  activate_mpl3115a2(measure_mode, precision_mode); 
}

/**
* @brief Permet d'écrire val dans le registre reg
* @param reg le registre de destination
* @param val la valeur à écrire
* @retval None
**/
void reg_write( u1_t reg, u2_t val){
  u2_t tab[2];
  tab[0] = reg;
  tab[1] = val;
  i2c_write(I2C1, ADDRESS_MPL3115A2, tab, 2);
}

/**
* @brief Permet de lire un certain registre
* @param reg le registre à lire
* @retval u2_t la valeur lue
**/
u2_t reg_read(u1_t reg){
  u2_t retour;
  i2c_start_read(I2C1, ADDRESS_MPL3115A2,reg);
  i2c_restart_read(I2C1, ADDRESS_MPL3115A2, &retour,1);
  return retour;
}


/**
* @brief Fonction appellée par @ref mpl3115a2_init pour definir le type de mesure
* @param measure_mode
* @param precision_mode
* @retval None
*/
void activate_mpl3115a2(u1_t measure_mode, u1_t precision_mode){
  
    I2C_AcknowledgeConfig(I2C1, ENABLE);
  //si on ne communique pas avec le bon capteur
  if(reg_read(REG_WHOAMI) != ID_MPL311A2){
    return;
  }
  //configuration des lectures en mode sleep
  reg_write(REG_CONFIG, measure_mode << 7 | precision_mode << 3 | BIT_STBY);
  //configuration des interruptions
  reg_write(REG_IT, 0x07);
  //activation des lectures
  reg_write(REG_CONFIG,  measure_mode << 7 | precision_mode << 3 | BIT_ACTIVE);
}

/**
* @brief Permet de tester la présence dans les registres d'une valeur valide, puis procède à la lecture de cette valeur
* @retval u4_t
*/
u4_t read_pres_register(){
  u2_t tab[5]={0,0,0};
  //Activation de l'ACK pour les messages
  while(!(reg_read((REG_STATUS)) & (0x08)));
  i2c_start_read(I2C1, ADDRESS_MPL3115A2,REG_MSB_1);
  i2c_restart_read(I2C1, ADDRESS_MPL3115A2, tab,3);
  return (tab[0]<<16 | tab[1]<<8 | tab[2])>>4;
}

/**
* @brief Permet de mettre de rajouter un offset sur les valeurs lues
* @param offset_type l'adresse du registre contenant l'offset (voir les defines)
* @param offset_value la valeur de l'offset
* @retval None
**/
void set_offset(u1_t offset_type, u1_t offset_value){
   reg_write(offset_type, offset_value);
}


/**
* @brief Permet de lire la pression sur le capteur
* @retval u4_t
**/
u4_t read_pressure(){
  return(read_pres_register()/4.0);
}

/**
* @brief Permet de lire l'altitude sur le capteur
* @retval None
**/
u4_t read_altitude(){
  return(read_pres_register()/16.0);
}

/**
* @brief Permet de lire la température sur le capteur
* @retval retourne la temperature en *10°C
**/
u4_t read_temperature(){
  u2_t tab[2]={0,0};
  float val;
  i2c_start_read(I2C1, ADDRESS_MPL3115A2,0x04);
  i2c_restart_read(I2C1, ADDRESS_MPL3115A2, tab,2); 
  val = (float) (tab[1] >> 4);
  val = tab[0]+val/16;
  return val*10;
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
