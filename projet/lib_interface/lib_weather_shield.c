/**
  ******************************************************************************
  * @file    lib_weather_shield.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion de certains capteurs de la Sparkfun Weather Shield
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/ 

#include "lib_weather_shield.h"
#include "lmic.h"
#include "hw.h"
#include "debug.h"
#include "stdint.h"
#include "lib_interupt.h"

/** @addtogroup Sensors_Drivers
 * @{
 */


/** @defgroup Weather_Shield
  * @brief Bibliothèque d'acquisition et de traitement des données de la Sparkfun Weather Shield
  * @{
  */


/* Private variables ---------------------------------------------------------*/
    
struct_weather_shield config_weather;
struct_weather_shield_datas data;

uint16_t direction_reference[16]; /*!<  Tableau de référence des tensions pour les directions*/

/**
* @brief Fonction donnée aux interruptions pour incrementer le vent
* @retval None
**/
void increment_wind(){
  data.wind_tics++;
}

/**
* @brief Fonction donnée aux interruptions pour incrementer le compteur de pluie
* @retval None
**/
void increment_rain(){
  data.rain_tics++;
}

/**
* @brief Permet d'initialiser la prise du vent (vitesse) et de la pluie
* @param init_struct la structure remplie d'initialisation
* @retval None
*/
void init_weather_shield(struct_weather_shield init_struct){
  config_weather = init_struct;
  //On active les clocks des GPIOs
  RCC->AHBENR  |= RCC_AHBENR_GPIOAEN;
  RCC->AHBENR  |= RCC_AHBENR_GPIOBEN;
  //Si la prise de vent est initialisée
  if(config_weather.wind_enable){
    //Activation du pin
    hw_cfg_pin(GPIOx(init_struct.wind_pin), init_struct.wind_pin, GPIOCFG_MODE_INP | GPIOCFG_OSPEED_40MHz | GPIOCFG_OTYPE_PUPD | GPIOCFG_PUPD_PUP);
    //activation de l'interruption associée
    hw_cfg_extirq(init_struct.wind_port, init_struct.wind_pin, GPIO_IRQ_RISING);
    //Configuration des interruptions associées
    set_pin_irq(init_struct.wind_pin, 10,increment_wind);
    //initialisation du compteur
    data.wind_tics = 0;
  }
  if(config_weather.rain_enable){
    hw_cfg_pin(GPIOx(init_struct.rain_pin), init_struct.rain_pin, GPIOCFG_MODE_INP | GPIOCFG_OSPEED_40MHz | GPIOCFG_OTYPE_PUPD | GPIOCFG_PUPD_PUP);
    hw_cfg_extirq(init_struct.rain_port, init_struct.rain_pin, GPIO_IRQ_RISING);
    set_pin_irq(init_struct.rain_pin, 30,increment_rain);
    data.rain_tics = 0;
  }
}

/**
* @brief Permet d'initiliser les références de voltage des directions de la girouette.
* @param tab_reference un tableau des 16 références correspondantes aux 16 points cardinaux
* @retval None
* @note L'étallonage peut être compliqué à réaliser
**/
void init_direction_reference(uint16_t tab_reference[]){
  int i;
  for(i=0; i < NUMBER_REFERENCE; i++){
    direction_reference[i] = tab_reference[i];
  }
}

/**
* @brief Permet de connaître le nombre de fermetures de l'anémomètre depuis l'initialisation
* @retval u4_t le nombre de fermeture de l'interrupteur de l'anémomètre
**/
u4_t get_wind_tics(){
  return data.wind_tics;
}

/**
* @brief Permet de remettre à zéro le compteur pour éviter un overflow (à utliser si l'application tourne depuis un certain temps)
**/
void reset_wind_tics(){
  data.wind_tics = 0;
}

/**
* @brief Permet de connaître le nombre de fermetures de l'interrupteur du pluviomètre
* @retval u4_t le nombre de fermeture de l'interrupteur du puviomètre
**/
u4_t get_rain_tics(){
  //pour renvoyer les vraies fermeture
  return data.rain_tics;
}

/**
* @brief Permet de remettre à zéro le compteur pour éviter un overflow (à utliser si l'application tourne depuis un certain temps)
**/
void reset_rain_tics(){
  data.rain_tics = 0;
}

/**
* @brief Permet d'obtenir l'intensité lumineuse
* @retval u2_t
**/
u2_t get_ligth(){
  uint16_t resultat = (*(config_weather.get_light_value))();
  return resultat;
}


/**
* @brief Permet d'obtenir la direction actuelle de la girouette. @ref init_direction_reference(uint16_t tab_reference[]) doit avoir été utilisée au préalable
* @retval u2_t une constante définie dans @ref lib_weather_shield.h indiquant la direction de la girouette
**/
u2_t get_direction(){
  uint16_t resultat = (*(config_weather.get_wind_direction_value))();
  debug_val("Resultat : ", resultat);
  int i;
  for(i=0; i < NUMBER_REFERENCE; i++){
    if(resultat <= direction_reference[i]+config_weather.delta && resultat >= direction_reference[i]-config_weather.delta){
      return i;
    }
  }
  return DIR_N;
}