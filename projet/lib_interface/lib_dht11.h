/**
  ******************************************************************************
  * @file    lib_dht11.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothèque implemente les fonctions nécéssaires pour 
  * l'utilisation du capteur DHT11.
  ******************************************************************************
  */

 /* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_DHT11_H
#define __LIB_DHT11_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx.h"
#include "oslmic.h"
#include "hw.h"
#include "debug.h"
#include "delay.h"

/** @addtogroup Sensors_Drivers
  * @{
  */


/** @addtogroup Digital_Sensors
  * @{
  */


/** @addtogroup DHT
  * @{
  */



/* Exported types ------------------------------------------------------------*/
/*
typedef enum {
    CELCIUS =0 ,
    FARENHEIT =1,
    KELVIN=2
} eScale;
*/


/* Exported constants --------------------------------------------------------*/

/** @defgroup Error_Codes
  * @{
  */
#define ERROR_NONE 0
#define BUS_BUSY 1
#define ERROR_NOT_PRESENT 2
#define ERROR_ACK_TOO_LONG 3
#define ERROR_SYNC_TIMEOUT 4
#define ERROR_DATA_TIMEOUT 5
#define ERROR_CHECKSUM 6
#define ERROR_NO_PATIENCE 7
/**
  * @}
  */ 

/** @defgroup DHT_Pin_Port
  * @{
  */
#define DHT11_PORT GPIOB
#define DHT11_PIN GPIO_Pin_12
/**
  * @}
  */ 

/* Exported functions ------------------------------------------------------- */

/* Fonction utilisée pour faire lire les données du capteur */
u1_t read_DHT(u1_t *dht_dat);

/* Fonctions utilisés pour selectionner les données voulues par l'utilisateur */
u1_t DHT11_Humidity(u1_t *buf);
u1_t DHT11_Temperature(u1_t *buf);


#endif

/**
  * @}
  */

/**
  * @}
  */
  
/**
  * @}
  */