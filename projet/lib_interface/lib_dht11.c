/**
  ******************************************************************************
  * @file    lib_dht11.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothèque implemente les fonctions nécéssaires pour 
  * l'utilisation du capteur DHT11.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "lib_dht11.h"
#include "stm32l1xx_conf.h"
#include "hw.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @defgroup DHT 
  * @brief DHT driver modules
  * @{
  */ 

/* Private define ------------------------------------------------------------*/
#define DHT_DATA_BIT_COUNT 41

/**
  * @brief  Réalise la lecture des 40 bits envoyés par le capteur et vérifie le respect du temps et du checksum du protocole utilisé.
  * @note   Le protocole est exigeant sur les temps utilisés. La fonction de délais doit donc être parfaitement fonctionnelle.
  * @param  dht_dat Structure de données où la lecture éfféctuée par le capteur sera sauvegardée
  * @retval u1_t Code d'erreur ou de succès renvoyé par l'execution
  */
u1_t read_DHT(u1_t *dht_dat){
  GPIO_InitTypeDef GPIO_InitStructure;
  s4_t i,j, retry_count, count, b;
  u4_t bit_times[DHT_DATA_BIT_COUNT];

  for (i = 0; i < DHT_DATA_BIT_COUNT; i++) {
        bit_times[i] = 0;
    }
  
  GPIO_InitStructure.GPIO_Pin = DHT11_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(DHT11_PORT, &GPIO_InitStructure);
  
  retry_count = 0;
  do {
        if (retry_count > 125) {
            return BUS_BUSY;
        }
        retry_count++;
        os_delay_micro(2);
    } while (!GPIO_ReadInputDataBit(DHT11_PORT, DHT11_PIN));


    // Send "start read and report" command to sensor....
    // First: pull-down I/O pin 
  GPIO_InitStructure.GPIO_Pin = DHT11_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(DHT11_PORT, &GPIO_InitStructure);

  GPIO_LOW(DHT11_PORT,DHT11_PIN);
  os_delay(18);
  
  GPIO_HIGH(DHT11_PORT,DHT11_PIN);
  os_delay_micro(40);
  
  //Next: Change pin to an input, to
  //watch for the 80us low explained a moment ago.
  GPIO_InitStructure.GPIO_Pin = DHT11_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(DHT11_PORT, &GPIO_InitStructure);
  
  retry_count = 0;
  do {
    if (retry_count > 40) {
      return ERROR_NOT_PRESENT;
    }
    retry_count++;
       
    os_delay_micro(1); 

  }while(GPIO_ReadInputDataBit(DHT11_PORT, DHT11_PIN));
    

  os_delay_micro(80);

  //now ready for data reception... pick up the 5 bytes coming from
  //   the sensor
  for (i=0; i<5; i++) {
    for (j = 0; j < 8; j++) {
      retry_count = 0;
      do {
        if (retry_count > 75)  {
                    return ERROR_DATA_TIMEOUT;
                }
                retry_count++;
                os_delay_micro(1);
      }while (!GPIO_ReadInputDataBit(DHT11_PORT, DHT11_PIN));

      os_delay_micro(40);
      bit_times[i*8+j] = GPIO_ReadInputDataBit(DHT11_PORT, DHT11_PIN);

      count = 0;
      while (GPIO_ReadInputDataBit(DHT11_PORT, DHT11_PIN) && count < 100) {
        os_delay_micro(1);
        count++;
      }
    }
  }

  //Next: restore pin to output duties
  GPIO_InitStructure.GPIO_Pin = DHT11_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(DHT11_PORT, &GPIO_InitStructure);

  //Next: Make data line high again, as output from Arduino
  GPIO_HIGH(DHT11_PORT,DHT11_PIN);

  for (i = 0; i < 5; i++) {
        b=0;
        for (j=0; j<8; j++) {
            if (bit_times[i*8+j+1] > 0) {
                b |= ( 1 << (7-j));
            }
        }
        dht_dat[i]=b;
    }

    debug_val ("b0 :", dht_dat[0]);
    debug_val ("b1 :", dht_dat[1]);
    debug_val ("b2 :", dht_dat[2]);
    debug_val ("b3 :", dht_dat[3]);
    debug_val ("b4 :", dht_dat[4]);
 
  //Next see if data received consistent with checksum received
  u1_t dht_check_sum =
         dht_dat[0]+dht_dat[1]+dht_dat[2]+dht_dat[3];
  /*Condition in following "if" says "if fifth byte from sensor
         not the same as the sum of the first four..."*/
  if(dht_dat[4]!= dht_check_sum)
     return ERROR_CHECKSUM;

  return ERROR_NONE;
};//end ReadDHT()

/**
  * @brief  Selectionne l'octet correspondant à l'humidité
  * @param  buf Le tampon contenant les 5 octets envoyés par le capteur
  * @retval u1_t La valeur attendue pour l'humidité
  */
u1_t DHT11_Humidity(u1_t *buf){
  return buf[0];
}

/**
  * @brief  Selectionne l'octet correspondant à la température
  * @param  buf Le tampon contenant les 5 octets envoyés par le capteur
  * @retval u1_t La valeur attendue pour la tempeéature 
  */
u1_t DHT11_Temperature(u1_t *buf){
  return buf[2];;
}

/**
  * @}
  */

/**
  * @}
  */
  
/**
  * @}
  */