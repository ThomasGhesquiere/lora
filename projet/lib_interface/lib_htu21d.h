/**
  ******************************************************************************
  * @file    lib_htu21d.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothèque implémente toutes les fonctions nécessaires pour l'intégration du capteur htu21d
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_HTU21D__
#define __LIB_HTU21D__

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_conf.h"
#include "oslmic.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/** @addtogroup HTU21D
  * @{
  */ 

/* Exported constants --------------------------------------------------------*/


/**
* @defgroup HTU21D_Def
* @brief Valeurs à definir pour le bon fonctionnement du capteur
* @{
*/
/**
* @brief Addresse du capteur
**/
#define ADDRESS_HTU21D 0x80
/**
* @brief 50Khz speed for I2C
**/
#define I2C_SPEED 50000

/**
  * @}
  */


/* Exported functions ------------------------------------------------------- */

/*Permet d'initialiser la connection I2C*/
void htu21d_init();
/*Permet de remplir les champs MSB, LSB et checksum demandés sur l'addresse */
void fill_fields(u1_t *MSB, u1_t *LSB, u1_t *checksum, u1_t adresse);

/*Récupère l'humidité */
u4_t get_humidity_htu21d();

/*Récupère la temperature */
u4_t get_temperature_htu21d();

#endif

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
