/**
  ******************************************************************************
  * @file    lib_onewire.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion du capteur ds1820.
  * @note    Dans cette biliothèque, seul le reset marche. Il n'est pas possible d'obtenir des valeurs
  * du ds1820. Attention, les délais utilisés doivent être assez précis pour générer des impulsions de l'ordre de la mircoseconde
  ******************************************************************************
  */


#include "stm32l1xx_conf.h"
#include "lmic.h"
#include "hal.h"
#include "debug.h"
#include "delay.h"
#include "lib_onewire.h"
#include <stdbool.h>



void set_in_ds1820(){
  GPIO_InitTypeDef GPIO_InitStructure;
  /* GPIOD Periph clock enable */
  //RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

  /* Configure PD0 and PD1 or PD3 and PD7 in output pushpull mode */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void set_out_ds1820(){
  GPIO_InitTypeDef GPIO_InitStructure;
  /* GPIOD Periph clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
  GPIO_DeInit (GPIOB);
  /* Configure PD0 and PD1 or PD3 and PD7 in output pushpull mode */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
}

u4_t ds1820_read_data(){
  set_in_ds1820();
  debug_val("Read : ", GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12));   
  return 0;
}

void generate_low_pulse(){
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_RESET);
  os_delay_micro(TLOW0);
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_SET);
  os_delay_micro(TREC);
}

void generate_high_pulse(){
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_RESET);
  os_delay_micro(TLOW1);
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_SET);
  os_delay_micro(TSLOT-TLOW1+TREC);
}

void write(u1_t value){
  int i = 0;
  set_out_ds1820();
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_SET);
  os_delay_micro(50);
  for(i = 0; i < 8; i++){
    if((value & 0x1)){
      generate_high_pulse();
    }
    else{
      generate_low_pulse();
    }
    value >>= 1;
  }
  
}

void reset(){
  set_out_ds1820();
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_SET);
  os_delay_micro(1000);
  GPIO_WriteBit(GPIOB, GPIO_Pin_12 ,Bit_RESET);
  os_delay_micro(600);
  set_in_ds1820();
  //tant que l'on a pas le pull up de la résistance
  while(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12));
  //le capteur fait le reset
  while(!GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12));
  //ici le reset a été effectué
  os_delay_micro(420);
}

bool read_bit(){
  generate_low_pulse();
  os_delay_micro(2);
  set_in_ds1820();
  os_delay_micro(5);
  return GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12);
}

u1_t read_Byte(){
  int i=0;
  u1_t val=0;
  for(i=0; i<8;i++){
    if(read_bit()){
      val |= (1<<i);
    }
    os_delay_micro(120);
  }
  return val;
}

void ds1820_write_data(){
  hal_disableIRQs();
  u1_t tab[9];
  debug_str("begin fct \r\n");
  int tab_valeur[MAX];
  int i=0;
  reset();
  write(0xCC);
  write(0x44);
  set_in_ds1820();
  os_delay_micro(750);
  reset();
  write(0xCC);
  write(0xBE);
  
  for(i=0; i<9;i++){
    tab[i] = read_Byte(); 
  }
  for(i=0;i<9;i++){
    debug_val("Tab : ",tab[i]);
  }
}
