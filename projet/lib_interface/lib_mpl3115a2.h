/**
  ******************************************************************************
  * @file    lib_mpl3115a2.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion du capteur I²C mpl3115a2
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_MPL3115A2__
#define __LIB_MPL3115A2__

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_conf.h"
#include "oslmic.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/** @addtogroup MPL3115A2
  * @{
  */ 


/* Exported constants --------------------------------------------------------*/

/**
* @defgroup MPL3115A2_Def
* @brief Valeurs à definir pour le bon fonctionnement du capteur
* @{
*/
/**
* @brief Addresse du capteur
**/
#define ADDRESS_MPL3115A2 0xC1
/**
* @brief 50Khz speed for I2C 
**/
#define I2C_SPEED 50000
/** 
* @brief Identificateur unique du capteur
*/
#define ID_MPL311A2 0xC4
/**
* @}
*/


/**
* @defgroup Mode_Mesure
* @brief La donnée mesurée avec le capteur
* @{
*/
/**
* @brief Sélection du mode de mesure de l'altitude
**/
#define MODE_ALT 0x1
/**
* @brief Sélection du mode de mesure de la pression
**/
#define MODE_PRES 0x0
    
#define MODE_TEMP MODE_PRES
/**
* @}
*/

/**
* @defgroup Mode_OS_X
* @brief Sélection du mode de précision de la mesure 
* @note Plus la precision est poussée et plus la conversion est lente (cf datasheet)
* @{
*/
#define MODE_OS_1 0x0 
#define MODE_OS_2 0x1
#define MODE_OS_4 0x2
#define MODE_OS_8 0x3
#define MODE_OS_16 0x4
#define MODE_OS_32 0x5
#define MODE_OS_64 0x6
#define MODE_OS_128 0x7
/**
* @}
*/

/** 
* @defgroup Offset_Adresses
* @brief Adresse des offset des valeurs mesurées 
* @{
*/
#define OFF_PRESSURE 0x2B 
#define OFF_TEMPERATURE 0x2C
#define OFF_ALTITUDE 0x2D
/**
* @}
*/

/** 
* @defgroup Registres_Utiles
* @brief Adresse des registres utiles
* @{
*/
#define REG_STATUS 0x06
#define REG_MSB_1 0x01
#define REG_CSB_1 0x02
#define REG_LSB_1 0x03
#define REG_TEMP_MSB 0x04
#define REG_TEMP_LSB 0x05
#define REG_IT  0x13
#define REG_CONFIG 0x26
#define REG_WHOAMI 0x0C
/**
* @}
*/

/** 
* @defgroup Bits_Fonct
* @brief Bits qui indiquent l'état de fonctionnement
* @{
*/
#define BIT_STBY 0x0
#define BIT_ACTIVE 0x1
/**
* @}
*/


/* Exported functions ------------------------------------------------------- */

/*Permet d'initialiser la connection I2C*/
void mpl3115a2_init(u1_t measure_mode, u1_t precision_mode);

/*Permet de lire un registre avec les spécifications du mpl3115A2*/
u2_t reg_read(u1_t reg);

/*Permet de lire la temperature du capteur*/
u4_t read_temperature();

/*Permet de lire la pression du capteur*/
u4_t read_pressure();

/*Permet de lire l'altitude*/
u4_t read_altitude();

/*Permet d'initialiser les mesures */
void activate_mpl3115a2(u1_t measure_mode, u1_t precision_mode);

void set_offset(u1_t offset_type, u1_t offset_value);

#endif


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
