/**
  ******************************************************************************
  * @file    lib_interupt.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion des interruption des pins digitaux. 
  * @note Attention EXTI_IRQHandler doit être définie dans le projet et dans le Makefile (voir l'exemple)
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __LIB_INTERUPT_H__
#define __LIB_INTERUPT_H__

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup Digital_Interrupt
  * @{
  */

/* Exported functions ------------------------------------------------------- */

/**
* @brief Permet de choisir une fonction à appeler lors de la détéction d'une interruption
* @param pin_name le numéro de pin
* @param debounce l'intervalle de prise en compte de l'interruption
* @param irq_fct un pointeur de fonction sur la fonction appelée lors de l'interruption sur le pin
* @note Si une interruption survient à l'instant t, aucune interruption ne sera prise en compte entre t et t + debounce
* @retval None 
**/
void set_pin_irq(u1_t pin_name, u4_t debounce, void (*irq_fct)(void));

/**
* @brief Fonction appellée lors d'une interruption quelconque, celle-ci associe le pin résponsable de l'interruption et la fonction a appeler
* @retval None
* @note Cette fonction doit être définie dans la paramètres du projet, comme fonction liée aux interruptions (voir dossier /examples pour plus d'informations)
**/
void sensorirq();

#endif