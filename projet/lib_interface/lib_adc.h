/**
  ******************************************************************************
  * @file    lib_adc.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Bibliothèque de gestion des convertion ADCs
  ******************************************************************************
  */

/*
 * Bibliothèque d'interface entre lmic et les librairies de biliothèques stm32
 * Facilite l'utilisation des entrées Analogique
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIB_ADC_H__
#define __LIB_ADC_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_conf.h"
#include "lmic.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Analogical_Sensors
  * @{
  */

/** @addtogroup ADC_Sensors
  * @{
  */


/* Exported types ------------------------------------------------------------*/

typedef void (*ptr_fonct)();

/**
* @brief 
**/
typedef struct{
        uint16_t lowThreshold; /*<    */ 
        uint16_t highThreshold; /*<    */ 
} struct_threshold;

/**
* @brief
**/
typedef struct{
  ADC_InitTypeDef ADC_InitStructure; /*<    */ 
  GPIO_InitTypeDef GPIO_InitStructure; /*<    */ 
  NVIC_InitTypeDef NVIC_InitStructure; /*<    */ 
  uint8_t ADC_Channel; /*<    */ 
  uint32_t GPIO_Pin; /*<    */ 
  GPIO_TypeDef* GPIOX; /*<    */ 
        ptr_fonct irq_function; /*<    */ 
        struct_threshold threshold; /*<    */ 
        u1_t mode;
} struct_adc_t;


/* Exported constants --------------------------------------------------------*/


/** @defgroup Mode_Convertion 
  * @brief Permet de choisir le mode de convertion
  * @{
  */

/**
* @brief Mode continu : les convertions sont faites à la suite les unes des autres et une interruption est générée lorsqu'une donnée est disponible
**/
#define MODE_CONTINU 1
/**
* @brief Mode sur demande : La convertion est effectué lorsqu'on le demande
**/
#define MODE_ASK 0

/**
  * @}
  */


/* Exported functions ------------------------------------------------------- */

/*Permet de choisir à configurer comme entrée analogique*/
void init_adc(uint8_t ADC_Channel, uint32_t GPIO_Pin, GPIO_TypeDef* GPIOX, u1_t mode, uint16_t HighThreshold, uint16_t LowThreshold);
void init_adc_GPIO(GPIO_InitTypeDef* GPIO_Init, uint32_t GPIO_Pin, GPIO_TypeDef* GPIOX);
void init_adc_RCC();
void init_adc_config(ADC_InitTypeDef* ADC_InitStruct, uint8_t ADC_Channel,u1_t mode, struct_threshold threshold);
void init_adc_NVIC(NVIC_InitTypeDef* NVIC_InitStructure);
void set_treshold (uint16_t HighThreshold , uint16_t LowThreshold);
void set_threshold_high(uint16_t HighThreshold);
void set_threshold_low(uint16_t LowThreshold);
uint16_t get_adc_value(ADC_TypeDef* ADCx);
void init_irq_adc_handler(ptr_fonct pfunction);
void irq_adc_handler(void);

#endif


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */



