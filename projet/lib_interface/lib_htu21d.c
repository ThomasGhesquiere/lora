/**
  ******************************************************************************
  * @file    lib_htu21d.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Ce fichier contient l'implémentation de toutes les fonctions nécessaires au capteur htu21d
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/  
#include "lib_htu21d.h"
#include "debug.h"
#include "lib_i2c.h"

/** @addtogroup Sensors_Drivers
  * @{
  */

/** @addtogroup Digital_Sensors
  * @{
  */

/** @addtogroup I2C_Protocol
  * @{
  */

/** @defgroup HTU21D
  * @brief HTU21D driver modules
  * @{
  */ 

/* Private variables ---------------------------------------------------------*/
GPIO_InitTypeDef  GPIO_InitStruct; /*!< structure contenant les informations  sur le GPIO */
I2C_InitTypeDef  I2C_InitStruct; /*!< structure contenant les configurations de l'i2c */

/**
* @brief Permet d'initialiser la connection I2C et le GPIO
* @retval None
*/
void htu21d_init()
{
  i2c_init_pin(I2C1, GPIO_Speed_40MHz, 0, I2C_AcknowledgedAddress_7bit, 50000);
}


/**
* @brief Permet de remplir les champs MSB, LSB et checksum demandés sur l'addresse 
* @note  Le checksum n'est pas implémenté
* @param MSB 
* @param LSB 
* @param checksum 
* @param adresse 
* @retval None
*/
void fill_fields(u1_t *MSB, u1_t *LSB, u1_t *checksum, u1_t adresse){
  
  i2c_start_read(I2C1,ADDRESS_HTU21D,adresse);
  
  u2_t tab[2];
  i2c_restart_read(I2C1,ADDRESS_HTU21D,tab,2);
  *MSB=tab[0];
  *LSB=tab[1];
}

/**
* @brief Récupère l'humidité
* @retval u4_t La valeur de la humidité
**/
u4_t get_humidity_htu21d(){
  u1_t MSB;
  u1_t LSB;
  u1_t checksum;
  //on demande le remplissage des champs
  fill_fields(&MSB, &LSB, &checksum,  0xE5);
  //on passe au calcul
  return -6 + (125*((MSB << 8) | LSB )/(1<<16));
  
}

/**
* @brief Récupère la temperature 
* @retval u4_t La valeur de la temperature
* @note Attention il semble qu'il y a des problèmes avec les calculs flotants
**/
u4_t get_temperature_htu21d(){
  u1_t MSB;
  u1_t LSB;
  u1_t checksum;
  //on demande le remplissage des champs
  fill_fields(&MSB, &LSB, &checksum,  0xE3);
  //on passe au calcul
  return -46.85 + (175.72*((MSB << 8) | LSB )/(1<<16));
  
}


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

