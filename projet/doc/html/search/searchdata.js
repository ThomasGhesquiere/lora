var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvw",
  1: "abcdefgilnoprstuw",
  2: "bdlmos",
  3: "abcdfghilmnoprsu",
  4: "abcdefghijklmnoprstuw",
  5: "gi",
  6: "abcdefgilmnprstuw",
  7: "ado",
  8: "abcdeghilmoprstuvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules"
};

