var searchData=
[
  ['calc_5fpressure',['calc_pressure',['../group___g_y__65.html#gac3789742ec9c7e56048e833228443b82',1,'lib_gy_65.c']]],
  ['calc_5ftemp',['calc_temp',['../group___g_y__65.html#gab68e5df459d7447d25f58abcdde17a39',1,'lib_gy_65.c']]],
  ['calibration_5fgy_5f65',['calibration_gy_65',['../group___g_y__65.html#ga0ad10af36bf76c0edd9eb9c5759d9a03',1,'lib_gy_65.c']]],
  ['configure_5fadc',['configure_ADC',['../group___a_d_c___d_m_a___sensors.html#gac02b7775bcd0ed62c8940917da1374b5',1,'lib_adc_dma.c']]],
  ['configure_5fdma',['configure_DMA',['../group___a_d_c___d_m_a___sensors.html#gaf704c204a2fef5b38b3a94f28c462a69',1,'lib_adc_dma.c']]],
  ['configure_5fgpio',['configure_GPIO',['../group___a_d_c___d_m_a___sensors.html#ga797d38ea534dc6b0a1b15fdfdfb0753b',1,'lib_adc_dma.c']]]
];
