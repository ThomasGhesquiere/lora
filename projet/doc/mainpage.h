/** @file mainpage.h
* @brief La page principale de la doc
*
* $Header: /nfs/slac/g/glast/ground/cvs/workbook/pages/advanced_doxygen/usingDoxygen.htm,v 1.1.1.1 2007/06/29 15:03:16 chuckp Exp $
*/ 
/** @mainpage
*
* @section scen Scénario
* Ces bibliothèques et la platine pourraient permettre à terme la gestion de 30000 capteurs sans fil sur un campus en produisant des données toutes les 10 secondes.
* Des exemples de capteurs serait par exemple:
* - Mouvement, déplacement (event)
* - Mesure environnementale (température, humidité).
*
*
*
* <b>OBJECTIF</b> : Intégration des drivers de capteurs analogiques et digitaux dans l’implémentation LMIC de LoRaWAN.
*
*
* <hr>
* @section intro Introduction
* Ce paquetage contient la bibliothèque développée pour le projet de spécialité 2A - Ensimag,
* dans le cadre du projet Smart Campus. Le but étant de réaliser une couche entre la bibliothèque 
* LMIC de IBM pour la communication sans fil LoRa et la bibliothèque standard du
* microprocesseur STM32. L'objectif principal est de faciliter grandement l'utilisation de capteurs avec la platine SK-iM880 à la manière d'Arduino.
* 
* Toutes les documentations utilisées au cours du projet ont été ajoutées dans des dossiers séparés selon leur
* classe (sensors, bootloader). Les codes sources de la bibliothèque ainsi que des
* exemples sont situés dans le repertoire projet. Ci-dessous l'organisation des
* dossiers sur le présent dans le rendu:
*
* <hr>
* @section org Organisation
* - <b>doc_bootloader</b> : la documentation de ST sur le fonctionnement du bootloader, 
* les commandes et les adresses pour chaque composants.
* - <b>doc_sensors</b> : la documentation spécifique pour chaque capteurs ainsi que les branchements nécessaires à leur utilisation.
* - <b>doc_SK-iM880</b> : la documentation sur la platine implémentant la technologie LoRa et contenant le microprocesseur STM32 sur lequel notre travail s'est basé.
* - <b>projet</b> : tous les fichiers qui concernent le projet, la documentation associée, les fichiers
* sources des bibliothèques et les exemples d'utilisation.
* -# <em>app_bootloader</em> : l'application réalisée en python pour controller la plaque et charger un nouveau
* programme au format .hex.
* -# <em>doc</em> : la documentation des bibliothèques, générée avec doxygen en html et latex. Le fichier de configuration pour générer l'ensemble de la documentation est également présent
* -# <em>examples</em> : des programmes d'exemple, directement compilable et utilisable avec l'IDE IAR Embedded.
* -# <em>lib_interface</em> : les fichiers sources permettant l'utilisation des capteurs avec la platine.
* -# <em>lmic</em> : la bibliothèque standard d'IBM pour la platine et la technologie LoRa avec des fichiers
* ajoutés assurant le bon fonctionnement de notre bibliothèque.
* -# <em>stm32</em> : les fichiers de la bibliothèque STM32 utilisés dans les exemples.
* - <b>STM32L1xx_StdPeriph_Lib_V1.3.1</b> : toute la bibliothèque standard de STM32 avec la documentation
* nécéssaire pour comprendre le fonctionnement du microprocesseur, du STMLink, etc.
*
* <hr>
* @section util Utilisation
* @subsection ex Exemples
* Pour utiliser les exemples, il faut au préalable avoir installé l'IDE IAR Embedded Workshop, disponible 
* <a href=https://www.iar.com/iar-embedded-workbench/arm/>ici</a>. Cela effectué, la compilation de tous les exemples, 
* ainsi que la programmation de la platine via STM Link V2 doit se dérouller correctement. Avec un
* simple reset le programme commence à s'éxecuter.
*
* @subsection boot Bootloader
* Pour éxecuter le script de flashage du stm32, il faut se mettre sur un environemment Linux, avoir les outils
* python et la bibliothèque serial installés et l'outil stm32flash (source code et instructions disponible 
* <a href=https://code.google.com/p/stm32flash/>ici</a>) compilé et l'exécutable placé dans /usr/bin. Il est 
* également possible d'exécuter ce script à distance avec un accès SSH sur la machine controllant la platine. 
* Il est nécessaire de spécifier au script le port avec l'option -p (/dev/ttyUSBx sur linux), le fichier .hex utilisé avec l'option -f. Le prefix
* <em>sudo</em> est  obligatoire pour communiquer avec le port série. Ci-dessous un exemple d'utilisation :
*
* - <em>sudo python application_bootloader -f hello.hex -p /dev/ttyUSB0</em>
*
* Le tutoriel <a href=http://ged.msu.edu/angus/tutorials/using-putty-on-windows.html>suivant</a> explique comment utiliser SSH ainsi que
* le protocole SCP, nécéssaire au transfert des fichiers .hex vers la machine hébergeant
* le script python et la platine.
*
* @subsection doc Documentation
* La documentation est présente en tant que commentaire dans les fichiers créés. Pour la générer, il est nécessaire d'installer Doxygen .En utilisant Doxywizard charger le fichier de configuration projet/doc/doxywizard_conf.
* Avec une simple commande "Run Doxygen" sur l'onglet "Run" la doc html et latex sera créée dans le dossier projet/doc.
*
* <hr>
* @authors 
* Project 4 - Smart Campus
* - JAYLES Romain <<A HREF="mailto:romain.jayles@ensimag.grenoble-inp.fr"> romain.jayles@ensimag.grenoble-inp.fr</A>>
* - GHESQUIERE Thomas <<A HREF="mailto:thomas.ghesquiere@ensimag.grenoble-inp.fr"> thomas.ghesquiere@ensimag.grenoble-inp.fr</A>>
* - SCHUH Matheus <<A HREF="mailto:matheus.schuh@ensimag.grenoble-inp.fr"> matheus.schuh@ensimag.grenoble-inp.fr</A>>
*
*/
