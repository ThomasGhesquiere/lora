/**
  ******************************************************************************
  * @file    delay.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothéque implémente les fonctions de délai en utilisant
  * les fonctionalités déjà implementés dans LMIC.
  * @note    Comme décrit dans le fichier oslmic.c ligne 99 "Illegal OSTICKS_PER_SEC - 
  * must be in range [10000:64516]. One tick must be 15.5us .. 100us long.", alors
  * nous n'avons pas réussi à vérifier le bon fonctionnement du delay en microseconds.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DELAY_H
#define __DELAY_H

/* Includes ------------------------------------------------------------------*/
#include "oslmic.h"
#include "lmic.h"
#include "hal.h"

/** @addtogroup LMIC
  * @{
  */

/** @addtogroup DELAY
  * @{
  */

/* Exported functions ------------------------------------------------------- */
void os_delay(u4_t msecs);
void os_delay_micro(u4_t usecs);

#endif

/**
  * @}
  */

/**
  * @}
  */
