/**
  ******************************************************************************
  * @file    delay.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothèque implémente les fonctions de délai en utilisant
  * les fonctionalités déjà implementées dans LMIC.
  * @note    Comme décrit dans le fichier oslmic.c ligne 99 "Illegal OSTICKS_PER_SEC - 
  * must be in range [10000:64516]. One tick must be 15.5us .. 100us long.", alors
  * nous n'avons pas réussi à vérifier le bon fonctionnement du delay en microseconds.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/  
#include "delay.h"

/** @addtogroup LMIC
  * @{
  */

/** @defgroup DELAY
  * @brief Delay functions utilisant la bibliothèque LMIC
  * @{
  */ 

/**
* @brief Cette fonction réalise un délai du nombre de miliseconds
* spécifié comme paramètre.
* @param msecs Le nombre de miliseconds désiré 
* @retval None
**/
void os_delay(u4_t msecs) {
  hal_waitUntil(os_getTime() + ms2osticks(msecs));
}

/**
* @brief Cette fonction réalise un délai du nombre de microseconds
* spécifié comme paramètre.
* @param usecs Le nombre de microseconds désiré 
* @retval None
**/
void os_delay_micro(u4_t usecs) {
  hal_waitUntil(os_getTime() + us2osticks(usecs));
}
