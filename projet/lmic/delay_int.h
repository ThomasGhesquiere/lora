/**
  ******************************************************************************
  * @file    delay_int.h
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothéque implémente les fonctions de délai en utilisant
  * des interruptions et les interfaces de la bibliothèque STM32 standard. 
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DELAY_INT_H
#define __DELAY_INT_H

/* Includes ------------------------------------------------------------------*/
#include "oslmic.h"
#include "stm32l1xx.h" 

/** @addtogroup LMIC
  * @{
  */

/** @addtogroup DELAY_INT
  * @{
  */

/* Exported functions ------------------------------------------------------- */
void delay_init(void);
void os_delay_micro(u4_t usecs);
void os_delay(u4_t msecs);
 
#endif

/**
  * @}
  */

/**
  * @}
  */
