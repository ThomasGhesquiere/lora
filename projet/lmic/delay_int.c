/**
  ******************************************************************************
  * @file    delay_int.c
  * @author  Project 4 - Smart Campus
  * @version V1.0
  * @date    08-Juin-2015
  * @brief   Cette bibliothéque implémente les fonctions de délai en utilisant
  * des interruptions et l'interface de la bibliothèque STM32 standard. 
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/  
#include "delay_int.h"
#include "stm32l1xx_rcc.h"
#include "stm32l1xx.h"
#include "misc.h"

/** @addtogroup LMIC
  * @{
  */

/** @defgroup DELAY_INT 
  * @brief Delay functions en utilisant des interruptions
  * @{
  */ 

/* Private variables ---------------------------------------------------------*/
static __IO uint32_t sysTickCounter;


/**
* @brief Cette fonction initialise le vecteur d'interruption, définie le tick 
* d'horloge pour chaque interruptions et également la fréquence de l'horloge.
* @retval None
**/
void delay_init(void) {
  /****************************************
   *SystemFrequency/1000      1ms         *
   *SystemFrequency/100000    10us        *
   *SystemFrequency/1000000   1us         *
  *****************************************/
  //set systick interrupt priority
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);    //4 bits for preemp priority 0 bit for sub priority
  NVIC_SetPriority(SysTick_IRQn, 0);//i want to make sure systick has highest priority amount all other interrupts
  
  RCC_ClocksTypeDef RCC_Clocks;
  
  debug_val("syscoreclk ", SystemCoreClock);
  

  RCC_GetClocksFreq(&RCC_Clocks);
  debug_val("hclk_frq ", RCC_Clocks.HCLK_Frequency);
  SysTick_Config(SystemCoreClock / 1000000); // One SysTick interrupt now equals 1us
}

/**
* @brief Cette fonction réalise un délai du nombre de microsecondes
* spécifié comme paramètre.
* @param usecs Le nombre de microseconds désirées
* @retval None
**/
void os_delay_micro(u4_t usecs) {
  sysTickCounter = usecs;
  while (sysTickCounter != 0) {
  }
}

/**
* @brief Cette fonction réalise un délai de 1 miliseconde
* et est utilisé par os_delay
* @retval None
**/
void delay_1ms(void) {
  sysTickCounter = 1000;
  while (sysTickCounter != 0) {
  }
}

/**
* @brief Cette fonction réalise un délai du nombre de milisecondes
* spécifié comme paramètre.
* @param msecs Le nombre de milisecondes désirées
* @retval None
**/
void os_delay(u4_t msecs) {
  while (msecs--) {
    delay_1ms();
  }
}

/**
* @brief Le handler appelé à chaque SysTick, s'ocuppe de décrémenter la variable de délais.
* @retval None
**/
void SysTick_Handler(void) {
  if (sysTickCounter != 0x00) {
    sysTickCounter--;
  }
}


/**
  * @}
  */

/**
  * @}
  */
