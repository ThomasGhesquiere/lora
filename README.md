# PROJET SMART CAMPUS #

## Scénario ##
Ces bibliothèques et la platine pourraient permettre à terme la gestion de 30000 capteurs sans fil sur un campus en produisant des données toutes les 10 secondes.
Des exemples de capteurs serait par exemple:

* Mouvement, déplacement (event).
* Mesure environnementale (température, humidité).

**OBJECTIF** : Intégration des drivers de capteurs analogiques et digitaux dans l’implémentation LMIC de LoRaWAN.

## Introduction ##
Ce paquetage contient la bibliothèque développée pour le projet de spécialité 2A - Ensimag, dans le cadre du projet Smart Campus. Le but étant de réaliser une couche entre la bibliothèque LMIC de IBM pour la communication sans fil LoRa et la bibliothèque standard du microprocesseur STM32. L'objectif principal est de faciliter grandement l'utilisation de capteurs avec la platine SK-iM880 à la manière d'Arduino.

Toutes les documentations utilisées au cours du projet ont été ajoutées dans des dossiers séparés selon leur classe (sensors, bootloader). Les codes sources de la bibliothèque ainsi que des exemples sont situés dans le repertoire projet. Ci-dessous l'organisation des dossiers sur le présent dans le rendu:

## Organisation ##
* **doc_bootloader** : la documentation de ST sur le fonctionnement du bootloader, les commandes et les adresses pour chaque composants.
* **doc_sensors** : la documentation spécifique pour chaque capteurs ainsi que les branchements nécessaires à leur utilisation.
* **doc_SK-iM880** : la documentation sur la platine implémentant la technologie LoRa et contenant le microprocesseur STM32 sur lequel notre travail s'est basé.
* **projet** : tous les fichiers qui concernent le projet, la documentation associée, les fichiers sources des bibliothèques et les exemples d'utilisation.
    * _app\_bootloader_ : l'application réalisée en python pour controller la plaque et charger un nouveau programme au format .hex.
    * _doc_ : la documentation des bibliothèques, générée avec doxygen en html et latex. Le fichier de configuration pour générer l'ensemble de la documentation est également présent
    * _examples_ : des programmes d'exemple, directement compilable et utilisable avec l'IDE IAR Embedded.
    * _lib\_interface_ : les fichiers sources permettant l'utilisation des capteurs avec la platine.
    * _lmic_ : la bibliothèque standard d'IBM pour la platine et la technologie LoRa avec des fichiers ajoutés assurant le bon fonctionnement de notre bibliothèque.
    * _stm32_ : les fichiers de la bibliothèque STM32 utilisés dans les exemples.
* **STM32L1xx\_StdPeriph\_Lib\_V1.3.1** : toute la bibliothèque standard de STM32 avec la documentation nécéssaire pour comprendre le fonctionnement du microprocesseur, du STMLink, etc.

## Utilisation ##
### Exemples ###
Pour utiliser les exemples, il faut au préalable avoir installé l'IDE IAR Embedded Workshop, disponible [ici](https://www.iar.com/iar-embedded-workbench/arm/). Cela effectué, la compilation de tous les exemples, ainsi que la programmation de la platine via STM Link V2 doit se dérouller correctement. Avec un simple reset le programme commence à s'éxecuter.

### Bootloader ###
Pour éxecuter le script de flashage du stm32, il faut se mettre sur un environemment Linux, avoir les outils python et la bibliothèque serial installés et l'outil stm32flash (source code et instructions disponible [ici](https://code.google.com/p/stm32flash/)) compilé et l'exécutable placé dans /usr/bin. Il est également possible d'exécuter ce script à distance avec un accès SSH sur la machine controllant la platine. Il est nécessaire de spécifier au script le port avec l'option -p (/dev/ttyUSBx sur linux), le fichier .hex utilisé avec l'option -f. Le prefix *sudo* est  obligatoire pour communiquer avec le port série. Ci-dessous un exemple d'utilisation :

* *sudo python application_bootloader -f hello.hex -p /dev/ttyUSB0*

Le tutoriel [suivant](http://ged.msu.edu/angus/tutorials/using-putty-on-windows.html) explique comment utiliser SSH ainsi que le protocole SCP, nécéssaire au transfert des fichiers .hex vers la machine hébergeant le script python et la platine.

### Documentation ###
La documentation est présente en tant que commentaire dans les fichiers créés. Pour la générer, il est nécessaire d'installer Doxygen .En utilisant Doxywizard charger le fichier de configuration projet/doc/doxywizard_conf. Avec une simple commande "Run Doxygen" sur l'onglet "Run" la doc html et latex sera créée dans le dossier projet/doc.

## Authors ##
Project 4 - Smart Campus

* JAYLES Romain <<romain.jayles@ensimag.grenoble-inp.fr>>
* GHESQUIERE Thomas <<thomas.ghesquiere@ensimag.grenoble-inp.fr>>
* SCHUH Matheus <<matheus.schuh@ensimag.grenoble-inp.fr>>