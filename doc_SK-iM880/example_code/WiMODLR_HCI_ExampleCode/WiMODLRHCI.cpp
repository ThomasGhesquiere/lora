//------------------------------------------------------------------------------
//
//  Include Files
//
//------------------------------------------------------------------------------

#include "WiMODLRHCI.h"
#include "CRC16.h"

//------------------------------------------------------------------------------
//
//  Class Constructor
//
//------------------------------------------------------------------------------

TWiMODLRHCI::TWiMODLRHCI()
{
    // register at ComSlip for message reception
    ComSlip.RegisterClient(this);

    // pass first RxBuffer and enable receiver/decoder
    ComSlip.SetRxBuffer(&RxMessage.SapID, (UINT16)WIMODLR_HCI_RX_MESSAGE_SIZE);

    // init counter
    RxCRCError = 0;
}

//------------------------------------------------------------------------------
//
//  PingRequest
//
//  @brief  send ping to check communication link
//
//------------------------------------------------------------------------------

TWiMODLRResult
TWiMODLRHCI::PingRequest()
{
    return SendMessage(DEVMGMT_SAP_ID, DEVMGMT_MSG_PING_REQ);
}

//------------------------------------------------------------------------------
//
//  SendMessage
//
//  @brief  send a message (with or without payload)
//
//------------------------------------------------------------------------------

TWiMODLRResult
TWiMODLRHCI::SendMessage(UINT8 sapID, UINT8 msgID, UINT8* payload, UINT16 length)
{
    // 1. check parameter
    //
    // 1.1 check length
    //
    if(length > WIMODLR_HCI_MSG_PAYLOAD_SIZE)
    {
        return WiMODLR_RESULT_PAYLOAD_LENGTH_ERROR;
    }
    // 1.2 check payload ptr
    //
    if(length && !payload)
    {
        return WiMODLR_RESULT_PAYLOAD_PTR_ERROR;
    }

    // 2.  init TxMessage
    //
    // 2.1 init SAP ID
    //
    TxMessage.SapID = sapID;

    // 2.2 init Msg ID
    //
    TxMessage.MsgID = msgID;

    // 2.3 copy payload, if present
    //
    if(payload && length)
    {
        UINT8*  dstPtr  = TxMessage.Payload;
        int     n       = (int)length;

        // copy bytes
        while(n--)
            *dstPtr++ = *payload++;
    }

    // 3. Calculate CRC16 over header and optional payload
    //
    UINT16 crc16 = CRC16_Calc(&TxMessage.SapID, length + WIMODLR_HCI_MSG_HEADER_SIZE, CRC16_INIT_VALUE);

    // 3.1 get 1's complement
    //
    crc16 = ~crc16;

    // 3.2 attach CRC16 and correct length, lobyte first
    //
    TxMessage.Payload[length++] = LOBYTE(crc16);
    TxMessage.Payload[length++] = HIBYTE(crc16);

    // 4. forward message to SLIP layer
    //    - start transmission with SAP ID
    //    - correct length by header size

    if (ComSlip.SendMessage(&TxMessage.SapID, length + WIMODLR_HCI_MSG_HEADER_SIZE))
    {
        // ok !
        return WiMODLR_RESULT_OK;
    }

    // SLIP layer wasn't able to sent
    return WiMODLR_RESULT_TRANMIT_ERROR;
}

//------------------------------------------------------------------------------
//
//  ProcessRxMessage
//
//  @brief: handle rx message
//
//------------------------------------------------------------------------------

UINT8*
TWiMODLRHCI::ProcessRxMessage(UINT8* rxBuffer, UINT16 length)
{
    // 1. check CRC
    if (CRC16_Check(rxBuffer, length, CRC16_INIT_VALUE))
    {
        // 2. check min length, 2 bytes for SapID + MsgID + 2 bytes CRC16
        if(length >= (WIMODLR_HCI_MSG_HEADER_SIZE + WIMODLR_HCI_MSG_FCS_SIZE))
        {
            // 3. Hack: since only one RxMessage buffer is used,
            //          rxBuffer must point to RxMessage.SapId, thus
            //          memcpy to RxMessage structure is not needed here

            // add length
            RxMessage.Length = length - (WIMODLR_HCI_MSG_HEADER_SIZE + WIMODLR_HCI_MSG_FCS_SIZE);

            // dispatch completed RxMessage
            DispatchRxMessage(RxMessage);
        }
    }
    else
    {
        // handle CRC error
        RxCRCError++;
    }

    // return same buffer again, keep receiver enabled
    return &RxMessage.SapID;
}
//------------------------------------------------------------------------------
//
//  DispatchRxMessage
//
//  @brief: dispatch CRC checked HCI message
//------------------------------------------------------------------------------

void
TWiMODLRHCI::DispatchRxMessage(TWiMODLR_HCIMessage& rxMsg)
{
    // 1. forward message according to SapID
    switch(rxMsg.SapID)
    {
        case    DEVMGMT_SAP_ID:
                DispatchDeviceMgmtMessage(rxMsg);
                break;

        case    RADIOLINK_SAP_ID:
                DispatchRadioLinkMessage(rxMsg);
                break;

        default:
                // handle unsupported SapIDs here
                break;
    }
}

//------------------------------------------------------------------------------
//
//  DisaptchDeviceMgmtMessage
//
//  @brief: dispatch Device Management Access Point messages here
//
//------------------------------------------------------------------------------

void
TWiMODLRHCI::DispatchDeviceMgmtMessage(TWiMODLR_HCIMessage& rxMsg)
{
    switch(rxMsg.MsgID)
    {
        case    DEVMGMT_MSG_PING_RSP:
                // handle ping response here
                break;

        default:
                // handle unsupported MsgIDs here
                break;
    }
}

//------------------------------------------------------------------------------
//
//  DispatchRadioLinkMessage
//
//  @brief: dispatch Radio Link Access Point messages here
//
//------------------------------------------------------------------------------

void
TWiMODLRHCI::DispatchRadioLinkMessage(TWiMODLR_HCIMessage& rxMsg)
{
    switch(rxMsg.MsgID)
    {

        default:
                // handle unsupported MsgIDs here
                break;
    }
}

//------------------------------------------------------------------------------
// end of file
//------------------------------------------------------------------------------
