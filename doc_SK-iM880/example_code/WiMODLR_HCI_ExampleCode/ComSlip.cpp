//------------------------------------------------------------------------------
//
//  Include Files
//
//------------------------------------------------------------------------------

#include "ComSlip.h"
#include "WiMODLRHCI.h"

//------------------------------------------------------------------------------
//
//  Protocol Definitions
//
//------------------------------------------------------------------------------

// SLIP Protocol Characters
#define SLIP_END					0xC0
#define	SLIP_ESC					0xDB
#define	SLIP_ESC_END				0xDC
#define	SLIP_ESC_ESC				0xDD

// SLIP Receiver/Decoder States
#define SLIPDEC_IDLE_STATE          0
#define	SLIPDEC_START_STATE			1
#define	SLIPDEC_IN_FRAME_STATE		2
#define	SLIPDEC_ESC_STATE			3


// interface maro to UART driver
extern void UART_SendByte(UINT8 txByte);

#define SendByte(txByte)            UART_SendByte(txByte)

//------------------------------------------------------------------------------
//
//  Class Constructor
//
//------------------------------------------------------------------------------

TComSlip::TComSlip()
{
    // init to idle state, no rx-buffer avaliable
    RxState         =   SLIPDEC_IDLE_STATE;
    RxIndex         =   0;
    RxBuffer        =   0;
    RxBufferSize    =   0;
    Client          =   0;
}

//------------------------------------------------------------------------------
//
//  RegisterClient
//
//  @brief: register upper layer client
//
//------------------------------------------------------------------------------

void
TComSlip::RegisterClient(TWiMODLRHCI *client)
{
    Client = client;
}

//------------------------------------------------------------------------------
//
//  SendMessage
//
//  @brief: send a message as SLIP frame
//
//------------------------------------------------------------------------------

bool
TComSlip::SendMessage(UINT8* msg, UINT16 msgLength)
{
    // send start of SLIP message
    SendByte(SLIP_END);

    // iterate over all message bytes
    while(msgLength--)
    {
        switch (*msg)
        {
                case SLIP_END:
                    SendByte(SLIP_ESC);
                    SendByte(SLIP_ESC_END);
                    break;

                case SLIP_ESC:
                    SendByte(SLIP_ESC);
                    SendByte(SLIP_ESC_ESC);
                    break;

                default:
                    SendByte(*msg);
                    break;
        }
        // next byte
        msg++;
    }

    // send end of SLIP message
    SendByte(SLIP_END);

    // always ok
    return true;
}

//------------------------------------------------------------------------------
//
//  SetRxBuffer
//
//  @brief: configure a rx-buffer and enable receiver/decoder
//
//------------------------------------------------------------------------------

bool
TComSlip::SetRxBuffer(UINT8* rxBuffer, UINT16  rxBufferSize)
{
    // receiver in IDLE state and client already registered ?
    if((RxState == SLIPDEC_IDLE_STATE) && Client)
    {
        // same buffer params
        RxBuffer        = rxBuffer;
        RxBufferSize    = rxBufferSize;

        // enable decoder
        RxState = SLIPDEC_START_STATE;

        return true;
    }
    return false;
}

//------------------------------------------------------------------------------
//
//  ProcessRxData
//
//  @brief: process received byte stream
//------------------------------------------------------------------------------

void
TComSlip::ProcessRxData(UINT8* rxData, UINT16 length)
{
    // iterate over all received bytes
    while(length--)
    {
        // get rxByte
        UINT8 rxByte = *rxData++;

        // decode according to current state
        switch(RxState)
        {
            case    SLIPDEC_START_STATE:
                    // start of SLIP frame ?
                    if(rxByte == SLIP_END)
                    {
                        // init read index
                        RxIndex = 0;

                        // next state
                        RxState = SLIPDEC_IN_FRAME_STATE;
                    }
                    break;

            case    SLIPDEC_IN_FRAME_STATE:
                    switch(rxByte)
                    {
                        case    SLIP_END:
                                // data received ?
                                if(RxIndex > 0)
                                {
                                    // yes, process rx message, get new buffer
                                    if (ProcessRxMsg())
                                    {
                                        RxState = SLIPDEC_START_STATE;
                                    }
                                    else
                                    {
                                        // disable decoder, temp. no buffer avaliable
                                        RxState = SLIPDEC_IDLE_STATE;
                                    }
                                }
                                // init read index
                                RxIndex = 0;
                                break;

                        case  SLIP_ESC:
                                // enter escape sequence state
                                RxState = SLIPDEC_ESC_STATE;
                                break;

                        default:
                                // store byte
                                StoreRxByte(rxByte);
                                break;
                    }
                    break;

            case    SLIPDEC_ESC_STATE:
                    switch(rxByte)
                    {
                        case    SLIP_ESC_END:
                                StoreRxByte(SLIP_END);
                                // quit escape sequence state
                                RxState = SLIPDEC_IN_FRAME_STATE;
                                break;

                        case    SLIP_ESC_ESC:
                                StoreRxByte(SLIP_ESC);
                                // quit escape sequence state
                                RxState = SLIPDEC_IN_FRAME_STATE;
                                break;

                        default:
                                // abort frame receiption
                                RxState = SLIPDEC_START_STATE;
                                break;
                    }
                    break;

            default:
                    break;
        }
    }
}

//------------------------------------------------------------------------------
//
//  StoreRxByte
//
//  @brief: store SLIP decoded rxByte
//
//------------------------------------------------------------------------------

void
TComSlip::StoreRxByte(UINT8 rxByte)
{
    if (RxIndex < RxBufferSize)
        RxBuffer[RxIndex++] = rxByte;
}

//------------------------------------------------------------------------------
//
//  ProcessRxMsg
//
//  @brief: forward decoded SLIP message to upper layer and get new rx-buffer
//
//------------------------------------------------------------------------------

bool
TComSlip::ProcessRxMsg()
{
    // forward received message to upper layer and allocate new rx-buffer
    if(Client)
    {
        RxBuffer = Client->ProcessRxMessage(RxBuffer, RxIndex);
    }

    // new buffer avaliable ?
    if (RxBuffer != 0)
    {
        // yes, keep receiver enabled
        return true;
    }

    // no further rx-buffer, stop receiver/decoder
    return false;
}
//------------------------------------------------------------------------------
// end of file
//------------------------------------------------------------------------------
