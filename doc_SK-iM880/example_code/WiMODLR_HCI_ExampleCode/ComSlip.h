#ifndef COMSLIP_H
#define COMSLIP_H

//------------------------------------------------------------------------------
//
// Include Files
//
//------------------------------------------------------------------------------

#include <inttypes.h>

//------------------------------------------------------------------------------
//
// General Definitions
//
//------------------------------------------------------------------------------

typedef uint16_t        UINT16;
typedef uint8_t         UINT8;

// forward declaration of upper layer class
class TWiMODLRHCI;

//------------------------------------------------------------------------------
//
// Class Declaration
//
//------------------------------------------------------------------------------

class TComSlip
{
    public:
                TComSlip();

    void        RegisterClient(TWiMODLRHCI*   client);

    bool        SetRxBuffer(UINT8*  rxBuffer, UINT16 rxbufferSize);

    bool        SendMessage(UINT8* msg, UINT16 msgLength);

    void        ProcessRxData(UINT8* rxData, UINT16 length);

    private:

    void        StoreRxByte(UINT8 rxByte);

    bool        ProcessRxMsg();



    private:

    // upper layer client
    TWiMODLRHCI*    Client;

    // receiver/decoder state
    int             RxState;

    // rx buffer index for next decodedrxByte
    UINT16          RxIndex;

    // size of RxBuffer
    UINT16          RxBufferSize;

    // pointer to RxBuffer
    UINT8*          RxBuffer;
};

#endif // COMSLIP_H

//------------------------------------------------------------------------------
// end of file
//------------------------------------------------------------------------------
