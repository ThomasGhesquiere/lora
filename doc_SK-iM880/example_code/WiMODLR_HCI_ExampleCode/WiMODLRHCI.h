#ifndef WIMODLRHCI_H
#define WIMODLRHCI_H

//------------------------------------------------------------------------------
//
// Include Files
//
//------------------------------------------------------------------------------

#include <inttypes.h>
#include "WiMODLRHCI_IDs.h"
#include "ComSlip.h"

//------------------------------------------------------------------------------
//
// General Declaration
//
//------------------------------------------------------------------------------

#define HIBYTE(w)       (UINT8)((w) >> 8)
#define LOBYTE(w)       (UINT8)(w)

typedef uint16_t        TWiMODLRResult;

//------------------------------------------------------------------------------
//
// HCI Message Declaration
//
//------------------------------------------------------------------------------

// message header size: 2 bytes for SapID + MsgID
#define WIMODLR_HCI_MSG_HEADER_SIZE     2

// message payload size
#define WIMODLR_HCI_MSG_PAYLOAD_SIZE    280

// frame check sequence field size: 2 bytes for CRC16
#define WIMODLR_HCI_MSG_FCS_SIZE        2

// visible max. buffer size for lower SLIP layer
#define WIMODLR_HCI_RX_MESSAGE_SIZE (WIMODLR_HCI_MSG_HEADER_SIZE\
                                    +WIMODLR_HCI_MSG_PAYLOAD_SIZE\
                                    +WIMODLR_HCI_MSG_FCS_SIZE)

typedef struct
{
    // Payload Length Information, not transmitted over UART interface !
    UINT16  Length;

    // Service Access Point Identifier
    UINT8   SapID;

    // Message Identifier
    UINT8   MsgID;

    // Payload Field
    UINT8   Payload[WIMODLR_HCI_MSG_PAYLOAD_SIZE];

    // Frame Check Sequence Field
    UINT8   CRC16[WIMODLR_HCI_MSG_FCS_SIZE];

}TWiMODLR_HCIMessage;


//------------------------------------------------------------------------------
//
// Definition of Result/Error Codes
//
//------------------------------------------------------------------------------

typedef enum
{
    WiMODLR_RESULT_OK = 0,
    WiMODLR_RESULT_PAYLOAD_LENGTH_ERROR,
    WiMODLR_RESULT_PAYLOAD_PTR_ERROR,
    WiMODLR_RESULT_TRANMIT_ERROR
}TWiMDLRResultcodes;

//------------------------------------------------------------------------------
//
// Class Declaration
//
//------------------------------------------------------------------------------

class TWiMODLRHCI
{
    public:
                        TWiMODLRHCI();

    TWiMODLRResult      PingRequest();

    UINT8*              ProcessRxMessage(UINT8* rxBuffer, UINT16 length);

    private:
    TWiMODLRResult      SendMessage(UINT8 sapId, UINT8 msgID, UINT8* payload = 0, UINT16 length = 0);

    void                DispatchRxMessage           (TWiMODLR_HCIMessage& rxMsg);
    void                DispatchDeviceMgmtMessage   (TWiMODLR_HCIMessage& rxMsg);
    void                DispatchRadioLinkMessage    (TWiMODLR_HCIMessage& rxMsg);

    // reserve one Tx-Message-Buffer
    TWiMODLR_HCIMessage TxMessage;

    // reserve one Rx-Message-Buffer
    TWiMODLR_HCIMessage RxMessage;

    // SLIP communication layer instance
    TComSlip            ComSlip;

    // CRC error counter
    int                 RxCRCError;
};

#endif // WIMODLRHCI_H

//------------------------------------------------------------------------------
// end of file
//------------------------------------------------------------------------------
